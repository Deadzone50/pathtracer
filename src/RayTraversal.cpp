#include "PathTracer.h"

#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

bool
PathTracer::ShadowRaycast4(const Vec3f &Origin, const Vec3f &Dir, std::vector<Node4*> &ToCheck4, float tMax)
{
	++RayCount;

	Vec3f InvDir;				//precompute inverse of direction
	InvDir.x = 1.0f/Dir.x;		//floating point division by zero is defined
	InvDir.y = 1.0f/Dir.y;
	InvDir.z = 1.0f/Dir.z;

	Vec3i Swap;					//precalculate if the bounds have to be swapped
	Swap.x = (Dir.x < 0);
	Swap.y = (Dir.y < 0);
	Swap.z = (Dir.z < 0);

	ToCheck4.push_back(BVH4_[0]);
	while(ToCheck4.size() != 0)
	{
		Node4 *N = ToCheck4.back();
		ToCheck4.pop_back();

		if(N->Leaf)				//leafnode, check triangles
		{
			for(size_t i = N->I1; i < N->I2; ++i)
			{
				//++Trig;		//count number of triangle tested per ray
				float t = FLT_MAX;
				float u, v;
				if((Triangles_)[TrigI[i]].Intersect(Origin, Dir, t, u, v))
				{
					if(0.0f < t && t < tMax)
					{
						return true;
					}
				}
			}
		}
		else
		{
			float T1 = (N->AABB[Swap.x].x - Origin.x)*InvDir.x;		//intersection x	//Ro + Rd*t ,t[0,Tmax]
			float T2 = (N->AABB[1-Swap.x].x - Origin.x)*InvDir.x;

			float T1Y = (N->AABB[Swap.y].y - Origin.y)*InvDir.y;		//intersection y
			float T2Y = (N->AABB[1-Swap.y].y - Origin.y)*InvDir.y;

			float T1Z = (N->AABB[Swap.z].z - Origin.z)*InvDir.z;		//intersection z
			float T2Z = (N->AABB[1-Swap.z].z - Origin.z)*InvDir.z;

			T1 = MAX(T1, T1Y);
			T1 = MAX(T1, T1Z);
			if(T1 > tMax)				//check that it isn't too far
				continue;
			T2 = MIN(T2, T2Y);
			T2 = MIN(T2, T2Z);

			Hit4 H = IntersectsNode4(*N, Origin, InvDir, Swap, tMax);
			if(H.ChildH[0])
			{
				ToCheck4.push_back(N->Child1);
			}
			if(H.ChildH[1])
			{
				ToCheck4.push_back(N->Child2);
			}
			if(H.ChildH[2])
			{
				ToCheck4.push_back(N->Child3);
			}
			if(H.ChildH[3])
			{
				ToCheck4.push_back(N->Child4);
			}
		}
	}
	return false;
}


RaycastResult
PathTracer::Raycast4(const Vec3f &Origin, const Vec3f &Dir, std::vector<Node4*> &ToCheck4, float tMax)
{
	++RayCount;

	int imin = -1;
	float tmin = tMax, umin = 0.0f, vmin = 0.0f;

	Vec3f InvDir;				//precompute inverse of direction
	InvDir.x = 1.0f/Dir.x;		//floating point division by zero is defined
	InvDir.y = 1.0f/Dir.y;
	InvDir.z = 1.0f/Dir.z;

	Vec3i Swap;					//precalculate if the bounds have to be swapped
	Swap.x = (Dir.x < 0);
	Swap.y = (Dir.y < 0);
	Swap.z = (Dir.z < 0);

	ToCheck4.push_back(BVH4_[0]);
	while(ToCheck4.size() != 0)
	{
		Node4 *N = ToCheck4.back();
		ToCheck4.pop_back();

		if(N->Leaf)				//leafnode, check triangles
		{
			for(size_t i = N->I1; i < N->I2; ++i)
			{
				//++Trig;		//count number of triangle tested per ray
				float t = FLT_MAX;
				float u, v;
				if((Triangles_)[TrigI[i]].Intersect(Origin, Dir, t, u, v))
				{
					if(t > 0.0f && t < tmin)
					{
						imin = (int)i;
						tmin = t;
						umin = u;
						vmin = v;
					}
				}
			}
		}
		else
		{
			float T1 = (N->AABB[Swap.x].x - Origin.x)*InvDir.x;		//intersection x	//Ro + Rd*t ,t[0,Tmax]
			float T2 = (N->AABB[1-Swap.x].x - Origin.x)*InvDir.x;

			float T1Y = (N->AABB[Swap.y].y - Origin.y)*InvDir.y;		//intersection y
			float T2Y = (N->AABB[1-Swap.y].y - Origin.y)*InvDir.y;

			float T1Z = (N->AABB[Swap.z].z - Origin.z)*InvDir.z;		//intersection z
			float T2Z = (N->AABB[1-Swap.z].z - Origin.z)*InvDir.z;

			T1 = MAX(T1, T1Y);
			T1 = MAX(T1, T1Z);
			if(T1 > tmin)				//check that it isn't too far
				continue;
			T2 = MIN(T2, T2Y);
			T2 = MIN(T2, T2Z);

			Hit4 H = IntersectsNode4(*N, Origin, InvDir, Swap, tmin);
			if(H.ChildH[0])
			{
				ToCheck4.push_back(N->Child1);
			}
			if(H.ChildH[1])
			{
				ToCheck4.push_back(N->Child2);
			}
			if(H.ChildH[2])
			{
				ToCheck4.push_back(N->Child3);
			}
			if(H.ChildH[3])
			{
				ToCheck4.push_back(N->Child4);
			}
		}
	}
	RaycastResult Castresult;
	if(imin != -1)
	{
		//castresult = RaycastResult{Triangles_[TrigI[imin]], tmin, umin, vmin, orig + tmin*dir, orig, dir};
		Castresult = RaycastResult{&Triangles_[TrigI[imin]], tmin, umin, vmin, Origin + tmin*Dir, Origin, Dir};
	}
	else
	{
		Castresult = RaycastResult{nullptr, FLT_MAX, 0, 0, Origin + FLT_MAX*Dir, Origin, Dir};
	}
	return Castresult;
}

RaycastResult
PathTracer::Raycast(const Vec3f& Origin, const Vec3f& Dir, std::vector<Node*> &ToCheck, float tMax)
{
	++RayCount;

	Hit H, HL, HR;
	int imin = -1;
	float tmin = tMax, umin = 0.0f, vmin = 0.0f;

	Vec3f InvDir;				//precompute inverse of direction
	InvDir.x = 1.0f/Dir.x;		//floating point division by zero is defined
	InvDir.y = 1.0f/Dir.y;
	InvDir.z = 1.0f/Dir.z;

	Vec3i Swap;					//precalculate if the bounds have to be swapped
	Swap.x = (Dir.x < 0);
	Swap.y = (Dir.y < 0);
	Swap.z = (Dir.z < 0);

	H = IntersectsNode(*BVH_[0], Origin, InvDir, Swap, tmin);
	if(H.H)							//check if hit root node
	{
		ToCheck.push_back(BVH_[0]);
		while(ToCheck.size() != 0)
		{
			Node *N = ToCheck.back();
			ToCheck.pop_back();
			if(N->LeftChild != NULL)	//check if leafnode
			{
				HL = IntersectsNode(*N->LeftChild, Origin, InvDir, Swap, tmin);
				HR = IntersectsNode(*N->RightChild, Origin, InvDir, Swap, tmin);

				if(HL.H && HR.H)			//if both hit, check closest first
				{
					//Bhit++;
					if(HL.T1 < HR.T1)
					{
						ToCheck.push_back(N->RightChild);
						ToCheck.push_back(N->LeftChild);
					}
					else
					{
						ToCheck.push_back(N->LeftChild);
						ToCheck.push_back(N->RightChild);
					}
				}
				else if(HL.H)
				{
					//Lhit++;
					ToCheck.push_back(N->LeftChild);
				}
				else if(HR.H)
				{
					//Rhit++;
					ToCheck.push_back(N->RightChild);
				}
				else
				{
					//miss
				}
			}
			else
			{		//leafnode, check triangles
				for(size_t i = N->I1; i < N->I2; ++i)
				{
					//++Trig;		//count number of triangle tested per ray
					float t = FLT_MAX;
					float u, v;
					if((Triangles_)[TrigI[i]].Intersect(Origin, Dir, t, u, v))
					{
						if(t > 0.0f && t < tmin)
						{
							imin = (int)i;
							tmin = t;
							umin = u;
							vmin = v;
						}
					}
				}
			}
		}
	}
	//std::cout << "Triangles read:" << Trig << std::endl;
	//Thit = Bhit+Lhit+Rhit;
	//int tot = BVH.size();
	//std::cout << "Hits B, L, R, T:" << Bhit << ", " << Lhit << ", " << Rhit << ", " << Thit << ", " << tot << std::endl;
	RaycastResult Castresult;
	if(imin != -1)
	{
		//castresult = RaycastResult{Triangles_[TrigI[imin]], tmin, umin, vmin, orig + tmin*dir, orig, dir};
		Castresult = RaycastResult{&Triangles_[TrigI[imin]], tmin, umin, vmin, Origin + tmin*Dir, Origin, Dir};
	}
	else
	{
		Castresult = RaycastResult{nullptr, FLT_MAX, 0, 0, Origin + FLT_MAX*Dir, Origin, Dir};
	}
	return Castresult;
}
