#include <GL/glut.h>
#include "Sidebar.h"

void
Sidebar::CreateSidebar(SceneData &Data)
{
	Textbox L;
	Button B;
	//light strenght
	L.Min = {-0.5f, 0.1f};
	L.Max = {0.5f, 0.2f};
	L.Value = &Data.PointLight.Emission_.x;
	L.Text = "Light: ";
	MenuText.push_back(L);
	B.Min = {0.3f, -0.05f};
	B.Max = {0.9f, 0.05f};
	B.Text = "Light+";
	MenuButtons.push_back(B);
	B.Min = {-0.9f, -0.05f};
	B.Max = {-0.3f, 0.05f};
	B.Text = "Light-";
	MenuButtons.push_back(B);
	//Bounces
	L.Min = {-0.5f, 0.4f};
	L.Max = {0.5f, 0.5f};
	L.Value = &Data.MaxBounces;
	L.Text = "Bounces: ";
	MenuText.push_back(L);
	B.Min = {0.3f, 0.25f};
	B.Max = {0.9f, 0.35f};
	B.Text = "Bounces+";
	MenuButtons.push_back(B);
	B.Min = {-0.9f, 0.25f};
	B.Max = {-0.3f, 0.35f};
	B.Text = "Bounces-";
	MenuButtons.push_back(B);
	//save
	B.Min = {-0.9f, 0.8f};
	B.Max = {-0.5f, 0.9f};
	B.Text = "Save scene";
	MenuButtons.push_back(B);
}

void
Sidebar::DrawSidebar(Vec2i &ScreenSize, int SideBarWidth)
{
	glColor3f(0.7f,0.7f,0.7f);
	glBegin(GL_POLYGON);
	glVertex3f(-1.1f, -1.1f, 0);
	glVertex3f(1.1f, -1.1f, 0);
	glVertex3f(1.1f, 1.1f, 0);
	glVertex3f(-1.1f, 1.1f, 0);
	glEnd();

	auto MTEND = MenuText.end();
	for(auto it = MenuText.begin(); it != MTEND; ++it)
	{
		float x1,x2,y1,y2;
		x1 = it->Min.x;
		x2 = it->Max.x;
		y1 = it->Min.y;
		y2 = it->Max.y;

		glColor3f(0.3f,0.3f,0.3f);
		glBegin(GL_POLYGON);
		glVertex3f(x1, y1, 0);
		glVertex3f(x2, y1, 0);
		glVertex3f(x2, y2, 0);
		glVertex3f(x1, y2, 0);
		glEnd();

		glColor3f(1,1,1);
		glRasterPos2f(x1, (y1+y2)/2);
		std::string Text = it->Text + std::to_string(*it->Value);

		for(size_t i = 0; i < Text.size();++i)
		{
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, Text[i]);
		}
	}
	auto MBEND = MenuButtons.end();
	for(auto it = MenuButtons.begin(); it != MBEND; ++it)
	{
		float x1,x2,y1,y2;
		x1 = it->Min.x;
		x2 = it->Max.x;
		y1 = it->Min.y;
		y2 = it->Max.y;

		glColor3f(0.3f,0.3f,0.3f);
		glBegin(GL_POLYGON);
		glVertex3f(x1, y1, 0);
		glVertex3f(x2, y1, 0);
		glVertex3f(x2, y2, 0);
		glVertex3f(x1, y2, 0);
		glEnd();

		glColor3f(1,1,1);
		glRasterPos2f(x1, (y1+y2)/2);
		for(size_t i = 0; i < it->Text.size();++i)
		{
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, it->Text[i]);
		}
	}


}