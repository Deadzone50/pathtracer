#pragma once
#include "Vector/Vector.h"

struct Hit
{
	bool H;
	float T1;
	float T2;
};

struct Hit4
{
	bool ChildH[4];
	//float T1[4];
	//float T2[4];
};

struct Node
{
	Vec3f AABB[2];					// Axis-aligned bounding box
	int I1, I2;						// Indices in the global list
	Node *LeftChild = NULL;
	Node *RightChild = NULL;
};

struct Node4						//BVH4 format
{
	Vec3f AABB[2];					// Axis-aligned bounding box
	int I1, I2;						// Indices in the global list
	bool Leaf = false;
	Node4 *Child1 = NULL;
	Node4 *Child2 = NULL;
	Node4 *Child3 = NULL;
	Node4 *Child4 = NULL;
};

Hit IntersectsNode(const Node& N, const Vec3f &Origin, const Vec3f &InvDir, Vec3i &Swap, float TMax);
Hit4 IntersectsNode4(const Node4& N, const Vec3f &Origin, const Vec3f &InvDir, Vec3i &Swap, float TMax);