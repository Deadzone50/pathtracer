#include <vector>
#include <map>
#include <fstream>
#include <iostream>
#include <chrono>

#include "PathTracer.h"
#include "Scene.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

bool
RayPlaneCollisionTest(Vec3f PlaneN, Vec3f PlaneP, Vec3f RayOrigin, Vec3f RayDir, float &MaxLen)
{
	float d = -DOT(PlaneN, PlaneP);
	float t  = (-d -DOT(PlaneN, RayOrigin))/(DOT(PlaneN, RayDir));
	if(t >= 0 && t < MaxLen)
	{
		MaxLen = t;
		return true;
	}
	return false;
}

bool
Triangle::Intersect(const Vec3f &Origin, const Vec3f &Dir, float &t, float &u, float &v)
{
	if(RayPlaneCollisionTest(Normal, Vertices[0].P, Origin, Dir, t))		//fails if ray is parallel to triangle
	{
		Vec3f P = Origin + t*Dir;

		Vec3f AB = Vertices[1].P - Vertices[0].P; 
		Vec3f BC = Vertices[2].P - Vertices[1].P; 
		Vec3f CA = Vertices[0].P - Vertices[2].P; 
		Vec3f AP = P - Vertices[0].P; 
		Vec3f BP = P - Vertices[1].P; 
		Vec3f CP = P - Vertices[2].P; 

		Vec3f ABP = CROSS(AB, AP);
		Vec3f BCP = CROSS(BC, BP);
		Vec3f CAP = CROSS(CA, CP);

		if (DOT(Normal, ABP) > 0 && 
			DOT(Normal, BCP) > 0 && 
			DOT(Normal, CAP) > 0)
		{
			float Area = LEN(CROSS(AB, BC));

			u = LEN(CAP) / Area;
			v = LEN(ABP) / Area;

			return true;		//P is inside the triangle
		}
	}
	return false;
}

void
ReadImageTexture(std::string Name, Texture &Tex)
{
	Tex.ImageData = stbi_load(Name.c_str(), &Tex.W, &Tex.H, &Tex.BPP, 0);		//3 = RGB
}

void
ParseMaterials(std::string FullPath, std::string Name, std::vector<Material> &Materials, std::map<std::string, size_t> &MaterialMap)
{
	std::string MaterialName;
	std::string Line;
	std::string Word;
	Vec3f V3;
	float F1;

	std::ifstream file;
	file.open(FullPath+Name);											//parse materials
	while(std::getline(file, Line))
	{
		if((Line[0] == '#') || (Line[0] == '\0'))				//skip comments and empty lines
			continue;
		std::istringstream iss(Line);
		iss >> Word;
		if(Word == "newmtl")	//material name
		{
			iss >> MaterialName;
			Material M = {0};
			M.TextureMaps = std::vector<Texture>(MaxTextureMaps);
			Materials.push_back(M);
			MaterialMap[MaterialName] = Materials.size()-1;

			continue;
		}
		else if (Word == "Ka")	//ambient color
		{
			continue;			//just ignore for now
		}
		else if (Word == "Kd")	//diffuse color
		{
			iss >> V3;
			Materials[MaterialMap[MaterialName]].Diffuse = V3;
			continue;
		}
		else if (Word == "Ks")	//specular color
		{
			iss >> V3;
			Materials[MaterialMap[MaterialName]].Specular = V3;
			continue;
		}
		else if (Word == "Ns")	//specular exponent
		{
			iss >> F1;
			Materials[MaterialMap[MaterialName]].Glossiness = F1;
			continue;
		}
		else if (Word == "Ke")	//emissive color
		{
			iss >> V3;
			Materials[MaterialMap[MaterialName]].Emission = V3;
			continue;
		}
		else if (Word == "d")	//dissolve
		{
			iss >> F1;
			Materials[MaterialMap[MaterialName]].Alpha = F1;
			continue;
		}
		else if (Word == "Tr")	//transparency
		{
			iss >> F1;
			Materials[MaterialMap[MaterialName]].Alpha = 1-F1;
			continue;
		}
		else if (Word == "Ni")	//index of refraction
		{
			iss >> F1;
			Materials[MaterialMap[MaterialName]].IndexOfRefraction	= F1;
			continue;
		}
		else if (Word == "Tf")	//transmission filter, Any light passing through the object is filtered by the transmission filter
		{
			iss >> Word;
			if(Word == "xyz" || Word == "spectral")
			{
				abort();			//unknown material property
			}
			else
			{
				iss = std::istringstream(Line);			//reset line
				iss >> Word;							//word is now "Tf" again

				iss >> V3;
				Materials[MaterialMap[MaterialName]].TransparencyFilter	= V3;
				continue;
			}
		}
		else if (Word == "map_Ka")
		{
			//iss >> Materials[MaterialMap[MaterialName]].map_Ka;
			continue;
		}
		else if (Word == "map_Ks")
		{
			std::string TexName;
			iss >> TexName;
			std::string FullTexName = FullPath+"/"+TexName;
			ReadImageTexture(FullTexName, Materials[MaterialMap[MaterialName]].TextureMaps[map_Ks]);
			continue;
		}
		else if (Word == "map_Kd")													//TODO: multiple materials with same image, what to do?
		{
			std::string TexName;
			iss >> TexName;
			std::string FullTexName = FullPath+"/"+TexName;
			ReadImageTexture(FullTexName, Materials[MaterialMap[MaterialName]].TextureMaps[map_Kd]);
			continue;
		}
		else if (Word == "map_d")
		{
			//iss >> Materials[MaterialMap[MaterialName]].map_d;
			continue;
		}
		else if (Word == "map_bump")
		{
			std::string TexName;
			iss >> TexName;
			std::string FullTexName = FullPath+"/"+TexName;
			ReadImageTexture(FullTexName, Materials[MaterialMap[MaterialName]].TextureMaps[map_bump]);
			continue;
		}
		else if (Word == "bump")
		{
			//iss >> Materials[MaterialMap[MaterialName]].bump;
			continue;
		}
		else if (Word == "disp")
		{
			std::string TexName;
			iss >> TexName;
			std::string FullTexName = FullPath+"/"+TexName;
			ReadImageTexture(FullTexName, Materials[MaterialMap[MaterialName]].TextureMaps[map_disp]);
			continue;
		}
		else if (Word == "illum")		//illumination model, not used
		{
			continue;				//discard
		}
		else
		{
			continue;			//unknown material property
		}
	}
	file.close();
}

void
LoadTrigMat(std::string FullPath,std::string Name, std::vector<Triangle> &Triangles, std::vector<Material> &Materials)
{
	std::vector<Vec3f> Vertexes;
	std::vector<Vec2f> TextureCordinates;
	std::vector<Vec3f> VertexNormals;
	std::string Line;
	std::string Word;
	Vec3f V3;
	Vec2f V2;

	std::string MaterialName;
	std::map<std::string, size_t> MaterialMap;

	std::ifstream file;
	file.open(FullPath+Name);
	if(!file.is_open())
	{
		std::cout << FullPath+Name << " not found\n";
	}

	while(std::getline(file, Line))
	{
		if((Line[0] == '#') || (Line[0] == '\0'))				//skip comments and empty lines
			continue;
		std::istringstream iss(Line);
		iss >> Word;
		if(Word == "v")				//vertex, assumes no w cordinate
		{
			iss >> V3;
			Vertexes.push_back(V3);
			continue;
		}
		else if(Word == "vt")		//texture coordinate, assumes no w cordinate
		{
			iss >> V2;
			TextureCordinates.push_back(V2);
			continue;
		}
		else if(Word == "vn")		//vertex normal
		{
			iss >> V3;
			VertexNormals.push_back(V3);
			continue;
		}
		else if (Word == "f")	//face element
		{
			int F[4] = {0};
			int FT[4] = {0};
			int FN[4] = {0};
			for(int i = 0; i<4; ++i)
			{
				iss >> F[i];				//extract the vertex index
				if (iss.peek() == '/')		//if there are texture, and normal indices
				{
					iss.ignore();
					if(iss.peek() == '/')	//normal index
					{
						iss.ignore();
						iss >> FN[i];
					}
					else
					{						//texture index
						iss >> FT[i];
						if(iss.peek() == '/')	//normal index
						{
							iss.ignore();
							iss >> FN[i];
						}
					}
				}
			}

			Triangle T;
			T.Vertices[0].P = Vertexes[F[0]-1];
			T.Vertices[1].P = Vertexes[F[1]-1];
			T.Vertices[2].P = Vertexes[F[2]-1];
			if(FN[0])
			{
				T.Vertices[0].N = VertexNormals[FN[0]-1];
				T.Vertices[1].N = VertexNormals[FN[1]-1];
				T.Vertices[2].N = VertexNormals[FN[2]-1];
			}
			if(FT[0])
			{
				T.Vertices[0].T = TextureCordinates[FT[0]-1];
				T.Vertices[1].T = TextureCordinates[FT[1]-1];
				T.Vertices[2].T = TextureCordinates[FT[2]-1];
			}
			Vec3f Normal = NORMALIZE(CROSS(T.Vertices[1].P - T.Vertices[0].P, T.Vertices[2].P - T.Vertices[0].P));
			T.Normal = Normal;
			T.Mat = &Materials[MaterialMap[MaterialName]];
			Triangles.push_back(T);
			if(!F[3])				//only 3 vertices read
			{
				continue;
			}
			Triangle T2;
			T2.Vertices[0].P = Vertexes[F[2]-1];
			T2.Vertices[1].P = Vertexes[F[3]-1];
			T2.Vertices[2].P = Vertexes[F[0]-1];
			if(FN[0])
			{
				T2.Vertices[0].N = VertexNormals[FN[2]-1];
				T2.Vertices[1].N = VertexNormals[FN[3]-1];
				T2.Vertices[2].N = VertexNormals[FN[0]-1];
			}
			if(FT[0])
			{
				T2.Vertices[0].T = TextureCordinates[FT[2]-1];
				T2.Vertices[1].T = TextureCordinates[FT[3]-1];
				T2.Vertices[2].T = TextureCordinates[FT[0]-1];
			}
			Normal = NORMALIZE(CROSS(T2.Vertices[1].P - T2.Vertices[0].P, T2.Vertices[2].P - T2.Vertices[0].P));
			T2.Normal = Normal;
			T2.Mat = &Materials[MaterialMap[MaterialName]];
			Triangles.push_back(T2);
			continue;
		}
		else
		{							//parse strings
			if(Word == "usemtl")
			{
				iss >> MaterialName;
				continue;
			}
			else if(Word == "mtllib")
			{
				iss >> Word;
				ParseMaterials(FullPath, Word, Materials, MaterialMap);
				continue;
			}
			else
			{
				continue;
				abort();			//unknown obj property
			}
		}
	}
	file.close();
}

void WriteTriangle(std::ofstream &file, Triangle &T, std::vector<Material> &Materials)
{
	size_t SIZE = Materials.size();
	size_t i = 0;
	for(; i < SIZE; ++i)					//find which material
	{
		if(T.Mat == &Materials[i])
		{
			break;
		}
	}

	Write(file, T.Vertices);
	Write(file, T.Normal);
	Write(file, i);
}

void ReadTriangle(std::istream &file, Triangle &T, std::vector<Material> &Materials)
{
	size_t i;
	Read(file, T.Vertices);
	Read(file, T.Normal);
	Read(file, i);

	T.Mat = &Materials[i];
}

void PathTracer::SaveScene(std::string Name)
{
	std::ofstream file(Name, std::ios::binary);

	AreaLight *AL = GetAreaLight();
	Write(file, AL->GetPosition());
	Write(file, AL->GetNormal());
	Write(file, AL->GetSize());
	Write(file, AL->GetEmission());

	Camera *Cam = GetCamera();
	Write(file, *Cam);

	//size_t MSIZE = Materials_.size();
	//Write(file, MSIZE);
	//for(size_t i = 0; i < MSIZE; ++i)
	//{
	//	Write(file, Materials_[i]);
	//	Write(file, Materials_[i].TextureMaps);
	//}
	size_t TSIZE = Triangles_.size();
	Write(file, TSIZE);
	for(int i = 0; i < TSIZE; ++i)
	{
		WriteTriangle(file, Triangles_[i], Materials_);
	}
	
}

void PathTracer::LoadScene(std::string Name)
{
	std::ifstream file(Name, std::ios::binary);

	Vec3f LPos, LDir;
	Vec3i LEmission;
	Vec2f LSize;
	Read(file, LPos);
	Read(file, LDir);
	Read(file, LSize);
	Read(file, LEmission);

	AreaLight *AL = GetAreaLight();
	PointLight *PL = GetPointLight();

	AL->SetPosition(LPos);
	AL->SetNormal(LDir);
	AL->SetSize(LSize);
	AL->SetEmission(LEmission);

	PL->SetPosition(LPos);
	PL->SetEmission(LEmission);

	Camera Cam;
	Read(file, Cam);
	SetCamera(Cam);

	//size_t MSIZE;
	//Read(file, MSIZE);
	//Materials_.resize(MSIZE);
	//for(size_t i = 0; i < MSIZE; ++i)
	//{
	//	Materials_[i].TextureMaps = std::vector<Texture>(MaxTextureMaps);
	//	Read(file, Materials_[i]);
	//	Read(file, Materials_[i].TextureMaps);
	//}
	size_t TSIZE;
	Read(file, TSIZE);
	Triangles_.resize(TSIZE);
	for(int i = 0; i < TSIZE; ++i)
	{
		ReadTriangle(file, Triangles_[i], Materials_);
	}
}

void PathTracer::ClearScene()
{
	std::cout << "Clearing old scene\n";
	Triangles_.clear();								//clear previous scene
	for(auto it = Materials_.begin(); it != Materials_.end(); ++it)
	{
		for(auto it2 = it->TextureMaps.begin(); it2 != it->TextureMaps.end(); ++it2)
		{
			stbi_image_free(it2->ImageData);
		}
	}
	Materials_.clear();
	for(auto it = BVH_.begin(); it != BVH_.end(); ++it)
	{
		delete (*it);
	}
	BVH_.clear();

	for(auto it = BVH4_.begin(); it != BVH4_.end(); ++it)
	{
		delete (*it);
	}
	BVH4_.clear();

	TrigI.clear();
	TrigIx.clear();
	TrigIy.clear();
	TrigIz.clear();
}

inline bool FileExsists (const std::string &Name)
{
	std::ifstream file(Name);
	if(file.is_open())
	{
		file.close();
		return true;
	}
	else
	{
		return false;
	}   
}

void PathTracer::OpenScene(std::string Path, std::string Name, std::string Extension)
{
	ClearScene();

	std::cout << "Opening scene " +Path+Name+Extension << std::endl;
	auto TimeBegin = std::chrono::high_resolution_clock::now();

	if(FileExsists(Name+".scene"))
	{
		std::cout << "Parsing "+Path+Name+".mtl" << std::endl;
		std::map<std::string, size_t> MaterialMap;
		ParseMaterials(Path, Name+".mtl", Materials_, MaterialMap);

		std::cout << "Opening "+Name+".scene" << std::endl;
		LoadScene(Name+".scene");
	}
	else
	{
		std::cout << "Parsing "+Name << "...\n";

		LoadTrigMat(Path, Name+Extension, Triangles_, Materials_);
		SaveScene(Name+".scene");
		auto TimeEnd = std::chrono::high_resolution_clock::now();

		float dt = std::chrono::duration<float>(TimeEnd - TimeBegin).count();
		std::stringstream ss;
		ss << dt*1000;
		std::string Time = ss.str() + " ms\n";
		std::cout << "Done, " << Time;
	}

	if(FileExsists(Name+".bvh"))
	{
		std::cout << "Opening "+Name+".bvh" << std::endl;
		LoadBVH(Name+".bvh");
		std::cout << "Done\n";
	}
	else
	{
		std::cout << "Generating "+Name+".bvh...\n";
		TimeBegin = std::chrono::high_resolution_clock::now();
		ConstructHierarchy(Triangles_);
		
		SaveBVH(Name+".bvh");
		auto TimeEnd = std::chrono::high_resolution_clock::now();

		float dt = std::chrono::duration<float>(TimeEnd - TimeBegin).count();
		std::stringstream ss;
		ss << dt*1000;
		std::string Time = ss.str() + " ms\n";
		std::cout << "Done, " << BVH_.size() << "nodes in " << Time;
	}
	TimeBegin = std::chrono::high_resolution_clock::now();
	std::cout << "Generating bvh4\n";
	ConvertToBVH4(BVH_[0]);
	auto TimeEnd = std::chrono::high_resolution_clock::now();
	float dt = std::chrono::duration<float>(TimeEnd - TimeBegin).count();
	std::stringstream ss;
	ss << dt*1000;
	std::string Time = ss.str() + " ms\n";
	std::cout << "Done in " << Time;

	std::cout << "Triangles in scene: " << Triangles_.size() << std::endl;
	std::cout << "BVH size: " << BVH_.size() << std::endl;
	std::cout << "BVH4 size: " << BVH4_.size() << std::endl;
}