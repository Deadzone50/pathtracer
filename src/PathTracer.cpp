#include <algorithm>			//std::sort
#include <fstream>
#include <iostream>
#include <cassert>

#include "PathTracer.h"

#define PI 3.1415926535897932384626f

void
AreaLight::Sample(float &pdf, Vec3f &Pos, Random &R)
{
	pdf = 1.0f/(4*Size_.x*Size_.y);
	Vec2f Rand = R.GetVec2f(-1, 1)*Size_;
	Pos = Position_ + Rand.x*Right_ + Rand.y*Up_;
}

Vec3f GetImageVec3f(const Texture &Image, Vec2i Pixel)
{
	//assert(Image.BPP == 3);

	Vec3f Result;
	uint8_t x = Image.ImageData[Pixel.y*Image.W*Image.BPP + Pixel.x*Image.BPP];
	uint8_t y = Image.ImageData[Pixel.y*Image.W*Image.BPP + Pixel.x*Image.BPP + 1];
	uint8_t z = Image.ImageData[Pixel.y*Image.W*Image.BPP + Pixel.x*Image.BPP + 2];
	//uint8_t a = Image.ImageData[Pixel.y*Image.W*Image.BPP + Pixel.x*Image.BPP + 3];			//alpha channel?

	Result.x = x /255.0f;		//scaled to [0 1]
	Result.y = y /255.0f;
	Result.z = z /255.0f;

	return Result;
}

Vec2i GetTexelCoords(Vec2f uv, Vec2i Size)
{
	Vec2f UVi;
	UVi.x = (int)uv.x;
	UVi.y = (int)uv.y;
	Vec2f Wrap = uv - UVi;			//wrapped to [-1,1]

	Wrap = Wrap + Vec2f{1,1};
	Vec2f Wrapi;
	Wrapi.x = (int)Wrap.x;
	Wrapi.y = (int)Wrap.y;
	Vec2f Wrap2 = Wrap - Wrapi;		//wrapped to [0,1]

	Vec2i Map;						//Map to pixels in image
	Map.x = Wrap2.x*Size.x;
	Map.y = (1-Wrap2.y)*(Size.y-1);		//images are mapped top to bottom, TODO: is this correct or is the problem somwhere else?

	return Map;
}

void
PathTracer::GetTextureParameters(const RaycastResult &Hit, Vec3f &Diffuse, Vec3f &Normal, Vec3f &Specular)
{
	//uv texture cordinates for hit
	Vec2f uv = (1 - Hit.u - Hit.v)*Hit.Tri->Vertices[0].T + Hit.u*Hit.Tri->Vertices[1].T + Hit.v*Hit.Tri->Vertices[2].T;

	if(Hit.Tri->Mat->TextureMaps[map_Kd].W)		//diffuse
	{
		const Texture &Image = Hit.Tri->Mat->TextureMaps[map_Kd];
		Vec2i TexelCoordinates = GetTexelCoords(uv, Vec2i{Image.W, Image.H});
		//Diffuse = GetImageVec3f(Image, TexelCoordinates);
		Vec3f D = Diffuse = GetImageVec3f(Image, TexelCoordinates);
		Diffuse = Vec3f{powf(D.x,2.2f), powf(D.y, 2.2f), powf(D.z, 2.2f)};		//gamma correction
	}
	else
	{
		Diffuse = Hit.Tri->Mat->Diffuse;
	}
	
	if(Hit.Tri->Mat->TextureMaps[map_Ks].W)		//specular
	{
		const Texture &Image = Hit.Tri->Mat->TextureMaps[map_Ks];
		Vec2i TexelCoordinates = GetTexelCoords(uv, Vec2i{Image.W, Image.H});
		Specular = GetImageVec3f(Image, TexelCoordinates);
	}
	else
	{
		Specular = Hit.Tri->Mat->Specular;
	}
	
	if(Hit.Tri->Mat->TextureMaps[map_bump].W)	//normal
	{
		Vec3f v0 = Hit.Tri->Vertices[0].P;
		Vec3f v1 = Hit.Tri->Vertices[1].P;
		Vec3f v2 = Hit.Tri->Vertices[2].P;

		Vec2f uv0 =  Hit.Tri->Vertices[0].T;
		Vec2f uv1 =  Hit.Tri->Vertices[1].T;
		Vec2f uv2 =  Hit.Tri->Vertices[2].T;

		Vec3f deltaPos1 = v1-v0;		//edges
		Vec3f deltaPos2 = v2-v0;

		Vec2f deltaUV1 = uv1-uv0;		// UV delta
		Vec2f deltaUV2 = uv2-uv0;

		float r = 1.0f / (deltaUV1.x*deltaUV2.y - deltaUV1.y*deltaUV2.x);
		Vec3f T = NORMALIZE((deltaPos1*deltaUV2.y - deltaPos2*deltaUV1.y)*r);
		Vec3f B =  NORMALIZE((-deltaPos2*deltaUV1.x - deltaPos1*deltaUV2.x)*r);

		Vec3f N = Hit.Tri->Normal;

		Mat3f TangentToWorld;
		TangentToWorld.SetCol(0, T);
		TangentToWorld.SetCol(1, B);
		TangentToWorld.SetCol(2, N);

		const Texture &Image = Hit.Tri->Mat->TextureMaps[map_bump];
		Vec2i TexelCoordinates = GetTexelCoords(uv, Vec2i{Image.W, Image.H});
		Normal = NORMALIZE(TangentToWorld * GetImageVec3f(Image, TexelCoordinates));
	}
	else
	{
		if(LEN(Hit.Tri->Vertices[0].N))		//if the mesh has normals defined
		{
			//interpolate normal
			Vec3f SN = NORMALIZE((1 - Hit.u - Hit.v)*Hit.Tri->Vertices[0].N + Hit.u*Hit.Tri->Vertices[1].N + Hit.v*Hit.Tri->Vertices[2].N);
			Normal = SN;
		}
		else
		{
			Normal = Hit.Tri->Normal;
		}
	}
}

bool
PathTracer::PathRender4(Vec2i ImageSize, std::vector<ImageVec3> &ImageBuffer, int SectionSize, int CurrentSection)	//BVH4 version
{
	//std::vector<DebugLine> DBL;
	const int Start = CurrentSection*SectionSize;			//imagesize must be evenly dividable by sectionsize
	const int StartX = Start % ImageSize.x;
	const int StartY = (Start / ImageSize.x)*SectionSize;

	if(StartY >= ImageSize.y)				//outside the image, we are done
		return true;

	int y = StartY;
	int x = StartX;

	int EndX, EndY;							//choose either end of section or image
	if(StartY+SectionSize < ImageSize.y)
		EndY = StartY+SectionSize;
	else
		EndY = ImageSize.y;

	if(StartX+SectionSize < ImageSize.x)
		EndX = StartX+SectionSize;
	else
		EndX = ImageSize.x;

	Camera *Cam = GetCamera();
	Vec3f RayOrigin = Cam->Pos;
	unsigned seed = std::chrono::high_resolution_clock::now().time_since_epoch().count() + CurrentSection;

	#pragma omp parallel					//create threads
	{
		Random R;
		#pragma omp critical(seed)			//different numbers for different threads
		{
			seed += 1;
			R.Seed(seed);
		}
		std::vector<Node4*> ToCheck4;
		ToCheck4.reserve(BVH4_.size());

		#pragma omp for						//split for-loop for threads
		for(y = StartY; y < EndY; ++y)
		{
			for(x = StartX; x < EndX; ++x)
			{
				// Generate a ray through the pixel.
				// NOTE that it's not normalized; the direction is defined
				// so that the segment to be traced is [Ro, Ro+Rd]
				float RandY = y + R.GetF32(-0.5f, 0.5f);				//randomize within pixel
				float Y1 = RandY / ImageSize.y * 2.0f - 1.0f;			//from -1 to 1
				RandY = y + R.GetF32(-0.5f, 0.5f);
				float Y2 = RandY / ImageSize.y * 2.0f - 1.0f;
				RandY = y + R.GetF32(-0.5f, 0.5f);
				float Y3 = RandY / ImageSize.y * 2.0f - 1.0f;

				float RandX = x + R.GetF32(-0.5f, 0.5f);
				float X1 = RandX / ImageSize.x * 2.0f - 1.0f;
				RandX = x + R.GetF32(-0.5f, 0.5f);
				float X2 = RandX / ImageSize.x * 2.0f - 1.0f;
				RandX = x + R.GetF32(-0.5f, 0.5f);
				float X3 = RandX / ImageSize.x * 2.0f - 1.0f;

				Vec3f PointOnImagePlane1 = Cam->Pos + 1*Cam->Dir + X1*Cam->Right + Y1*Cam->Up;
				Vec3f PointOnImagePlane2 = Cam->Pos + 1*Cam->Dir + X2*Cam->Right + Y2*Cam->Up;
				Vec3f PointOnImagePlane3 = Cam->Pos + 1*Cam->Dir + X3*Cam->Right + Y3*Cam->Up;

				Vec3f RayDirection1 = NORMALIZE(PointOnImagePlane1 - RayOrigin);
				Vec3f RayDirection2 = NORMALIZE(PointOnImagePlane2 - RayOrigin);
				Vec3f RayDirection3 = NORMALIZE(PointOnImagePlane3 - RayOrigin);

				Vec3f Color1 = PathTrace(RayOrigin, RayDirection1, ToCheck4, R);
				Vec3f Color2 = PathTrace(RayOrigin, RayDirection2, ToCheck4, R);
				Vec3f Color3 = PathTrace(RayOrigin, RayDirection3, ToCheck4, R);

				Vec3f Color = (Color1 + Color2 + Color3)/3.0f;


				ImageBuffer[ImageSize.x*y + x].Color += Color;
				ImageBuffer[ImageSize.x*y + x].Samples += 1;
			}
		}
	}
	return false;
}

bool
PathTracer::PathRender(Vec2i ImageSize, std::vector<ImageVec3> &ImageBuffer, int SectionSize, int CurrentSection)
{
	//std::vector<DebugLine> DBL;
	const int Start = CurrentSection*SectionSize;			//imagesize must be evenly dividable by sectionsize
	const int StartX = Start % ImageSize.x;
	const int StartY = (Start / ImageSize.x)*SectionSize;

	if(StartY >= ImageSize.y)				//outside the image, we are done
		return true;

	int y = StartY;
	int x = StartX;

	int EndX, EndY;							//choose either end of section or image
	if(StartY+SectionSize < ImageSize.y)
		EndY = StartY+SectionSize;
	else
		EndY = ImageSize.y;

	if(StartX+SectionSize < ImageSize.x)
		EndX = StartX+SectionSize;
	else
		EndX = ImageSize.x;

	Camera *Cam = GetCamera();
	Vec3f RayOrigin = Cam->Pos;
	unsigned seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();

	#pragma omp parallel					//create threads
	{
		#pragma omp atomic					//different numbers for different threads
		seed += 1;
		Random R(seed);
		std::vector<Node*> ToCheck;
		ToCheck.reserve(BVH_.size());
		//std::vector<Node4*> ToCheck4;
		//ToCheck4.reserve(BVH4_.size());

		#pragma omp for						//split for loop for threads
		for(y = StartY; y < EndY; ++y)
		{
			float RandY = y + R.GetF32(-0.5f, 0.5f);				//randomize within pixel
			float Y = RandY / ImageSize.y * 2.0f - 1.0f;
			for(x = StartX; x < EndX; ++x)
			{
				// Generate a ray through the pixel.
				// NOTE that it's not normalized; the direction is defined
				// so that the segment to be traced is [Ro, Ro+Rd]
				float RandX = x + R.GetF32(-0.5f, 0.5f);			//randomize within pixel
				float X = RandX / ImageSize.x * 2.0f - 1.0f;		//from -1 to 1

				Vec3f PointOnImagePlane = Cam->Pos + 1*Cam->Dir + X*Cam->Right + Y*Cam->Up;
				Vec3f RayDirection = FLT_MAX*NORMALIZE(PointOnImagePlane - RayOrigin);			//max lenght
				//Vec3f RayDirection = NORMALIZE(PointOnImagePlane - RayOrigin);			//max lenght

				Vec3f Color1 = PathTrace(RayOrigin, RayDirection, ToCheck, R);
				Vec3f Color2 = PathTrace(RayOrigin, RayDirection, ToCheck, R);

				//Vec3f Color1 = PathTrace(RayOrigin, RayDirection, ToCheck4, R);
				//Vec3f Color2 = PathTrace(RayOrigin, RayDirection, ToCheck4, R);

				Vec3f Color = (Color1 + Color2)/2.0f;


				ImageBuffer[ImageSize.x*y + x].Color += Color;
				ImageBuffer[ImageSize.x*y + x].Samples += 1;
			}
		}
	}
	return false;
}

Mat3f FormBasis(const Vec3f& n)							//forms base from direction
{
	Vec3f Q = n;
	if(abs(Q.x) <= abs(Q.y) && abs(Q.x) <= abs(Q.z))	//find smallest, replace with 1
		Q.x = 1;
	else if(abs(Q.y) <= abs(Q.x) && abs(Q.y) <= abs(Q.z))
		Q.y = 1;
	else
		Q.z = 1;

	Vec3f T = NORMALIZE(CROSS(Q, n));
	Vec3f B = CROSS(n, T);

	Mat3f R;
	R.SetCol(0, T);
	R.SetCol(1, B);
	R.SetCol(2, n);
	return R;
}

Vec3f
CosDir(Vec3f MappedNormal, Vec3f TriangleNormal, float Len, Random &R)		//generates random dirs
{
	Vec3f Dir;
	{
		float x = 10;
		float y = 10;
		while(pow(x, 2)+pow(y, 2) >= 1)				//generate point
		{
			x = R.GetF32(-1, 1);
			y = R.GetF32(-1, 1);
		}
		float z = sqrt(1-pow(x, 2)-pow(y, 2));		//lift to sphere
		Vec3f D = Vec3f{x, y, z};
		Mat3f Rot = FormBasis(MappedNormal);					//rotation matrix
		Dir = NORMALIZE(Rot*D)*Len;
		//if(m_normalMapped)
		//{
		//if(DOT(Dir, TriangleNormal) < 0)			//make sure it doesnt go into the surface
		//{
		//	Dir += DOT(-Dir, TriangleNormal)*TriangleNormal*1.01f;	//angle it along the surface, probably skrews with the pdf, TODO: fix
		//}
		//}
	}
	return Dir;
}

Vec3f
PathTracer::PathTrace(Vec3f RayOrigin, Vec3f RayDirection, std::vector<Node4*> &ToCheck4, Random &R)	//BVH4 version
{
	// trace!
	RaycastResult Result = Raycast4(RayOrigin, RayDirection, ToCheck4, FLT_MAX);

	// if we hit something, fetch a color and insert into image
	Vec3f Ei{0,0,0};
	Vec3f Throughput{1, 1, 1};
	int Bounce = 0;
	float Prob = 1;
	while(true)
	{
		if (Result.Tri != nullptr)
		{
			Vec3f DiffuseTex;
			Vec3f MappedNormal;
			Vec3f TriangleNormal = Result.Tri->Normal;
			Vec3f Specular;
			Vec3f Point = Result.Point;

			GetTextureParameters(Result, DiffuseTex, MappedNormal, Specular);
			//if(DOT(MappedNormal, Result.Dir) > 0)		//flip normal if wrong way
			if(DOT(TriangleNormal, Result.Dir) > 0)		//flip normal if wrong way
			{
				MappedNormal = -MappedNormal;
				TriangleNormal = -TriangleNormal;
			}

			float pdf;
			Vec3f LightPos;
			AreaLight *AL = GetAreaLight();
			AL->Sample(pdf, LightPos, R);
			Vec3f DirectionToLight = LightPos - Point;
			float r = LEN(DirectionToLight);
			DirectionToLight = DirectionToLight/r;
			//RaycastResult ShadowRes = Raycast4(Point+0.001f*MappedNormal, DirectionToLight, ToCheck4, r);

			//float materailpdf = cos(theta)/PI;
			Throughput *= DiffuseTex + Specular; //  * cos(theta)/PI / materialpdf

			//if(ShadowRes.Tri == nullptr)
			if(!ShadowRaycast4(Point+0.001f*MappedNormal, DirectionToLight, ToCheck4, r))
			{
				// if not, add the appropriate emission, 1/r^2 and clamped cosine terms, accounting for the PDF as well.
				// accumulate into E

				Vec3i LEmission = AL->GetEmission();
				Vec3f LNorm = AL->GetNormal();
				float cos1  = DOT(-DirectionToLight, LNorm)/(LEN(LNorm));					//xy and light normal
				float cos2  = DOT(DirectionToLight, MappedNormal)/(LEN(MappedNormal));		//incoming and surface normal
				if(cos1 < 0)
					cos1 = 0;
				if(cos2 < 0)
					cos2 = 0;

				Ei += ((cos1 * cos2)/(r*r * pdf))*LEmission * Prob * Throughput / PI;			//add contribution
			}
			Vec3f Dir = CosDir(MappedNormal, TriangleNormal, 1, R);

			if(SD_->RussianRoulette)						//russian roulette
			{
				if(Bounce < SD_->MaxBounces)		
				{
					++Bounce;						//continue
					//Result = Raycast4(Point+Dir*0.001f/FLT_MAX, Dir, ToCheck4);
					Result = Raycast4(Point+Dir*0.001f, Dir, ToCheck4, FLT_MAX);
				}
				else
				{
					float Rand = R.GetF32(0, 1);
					if(Rand < 0.2)
					{
						++Bounce;					//continue
						Prob *= 1/0.8;
						//Result = Raycast4(Point+Dir*0.001f/FLT_MAX, Dir, ToCheck4);
						Result = Raycast4(Point+Dir*0.001f, Dir, ToCheck4, FLT_MAX);
					}
					else
					{
						break;			//terminate
					}
				}
			}
			else
			{
				if(Bounce < SD_->MaxBounces)		//normal
				{
					++Bounce;					//continue
					//Result = Raycast4(Point+Dir*0.001f/FLT_MAX, Dir, ToCheck4);
					Result = Raycast4(Point+Dir*0.001f, Dir, ToCheck4, FLT_MAX);
				}
				else
				{
					break;			//terminate
				}
			}
		}
		else
		{
			break;		//missed
		}
	}
	return Ei;
}

Vec3f
PathTracer::PathTrace(Vec3f RayOrigin, Vec3f RayDirection, std::vector<Node*> &ToCheck, Random &R)
{
	// trace!
	RaycastResult Result = Raycast(RayOrigin, RayDirection, ToCheck, 1);

	// if we hit something, fetch a color and insert into image
	Vec3f Ei{0,0,0};
	Vec3f Throughput{1, 1, 1};
	int Bounce = 0;
	float Prob = 1;
	while(true)
	{
		if (Result.Tri != nullptr)
		{
			Vec3f DiffuseTex;
			Vec3f MappedNormal;
			Vec3f TriangleNormal = Result.Tri->Normal;
			Vec3f Specular;
			Vec3f Point = Result.Point;
			
			GetTextureParameters(Result, DiffuseTex, MappedNormal, Specular);
			if(DOT(MappedNormal, Result.Dir) > 0)		//flip normal if wrong way
			{
				MappedNormal = -MappedNormal;
				TriangleNormal = -TriangleNormal;
			}

			float pdf;
			Vec3f LightPos;
			AreaLight *AL = GetAreaLight();
			AL->Sample(pdf, LightPos, R);
			Vec3f DirectionToLight = LightPos - Point;
			RaycastResult ShadowRes = Raycast(Point+0.001f*MappedNormal, DirectionToLight, ToCheck, 1);

			//float materailpdf = cos(theta)/PI;
			Throughput *= DiffuseTex + Specular; //  * cos(theta)/PI / materialpdf
			if(ShadowRes.Tri == nullptr)
			{
				// if not, add the appropriate emission, 1/r^2 and clamped cosine terms, accounting for the PDF as well.
				// accumulate into E
				float r = LEN(DirectionToLight);
				Vec3i LEmission = AL->GetEmission();
				Vec3f LNorm = AL->GetNormal();
				float cos1  = DOT(-DirectionToLight, LNorm)/(LEN(LNorm) * r);					//xy and light normal
				float cos2  = DOT(DirectionToLight, MappedNormal)/(LEN(MappedNormal) * r);		//incoming and surface normal
				if(cos1 < 0)
					cos1 = 0;
				if(cos2 < 0)
					cos2 = 0;

				Ei += ((cos1 * cos2)/(r*r * pdf))*LEmission * Prob * Throughput / PI;			//add contribution
			}
			Vec3f Dir = CosDir(MappedNormal, TriangleNormal, FLT_MAX, R);
			if(SD_->RussianRoulette)						//russion roulette
			{
				if(Bounce < SD_->MaxBounces)		
				{
					++Bounce;						//continue
					Result = Raycast(Point+Dir*0.001f/FLT_MAX, Dir, ToCheck, 1);
				}
				else
				{
					float Rand = R.GetF32(0, 1);
					if(Rand < 0.2)
					{
						++Bounce;					//continue
						Prob *= 1/0.8;
						Result = Raycast(Point+Dir*0.001f/FLT_MAX, Dir, ToCheck, 1);
					}
					else
					{
						break;			//terminate
					}
				}
			}
			else
			{
				if(Bounce < SD_->MaxBounces)		//normal
				{
					++Bounce;					//continue
					Result = Raycast(Point+Dir*0.001f/FLT_MAX, Dir, ToCheck, 1);
				}
				else
				{
					break;			//terminate
				}
			}
		}
		else
		{
			break;		//missed
		}
	}
	return Ei;
}

Vec3f PathTracer::DebugPath4(int ImageX, int ImageY, Vec2i ImageSize, std::vector<DebugLine> &DebugLines, Random &R)
{
	std::vector<Node4*> ToCheck4;
	ToCheck4.reserve(BVH4_.size());

	float x = (float)ImageX / ImageSize.x * 2.0f - 1.0f;		//from -1 to 1
	float y = (float)ImageY / ImageSize.y * 2.0f - 1.0f;

	Camera *Cam = GetCamera();
	Vec3f RayOrigin = Cam->Pos;
	Vec3f PointOnImagePlane = Cam->Pos + 1*Cam->Dir + x*Cam->Right + y*Cam->Up;
	Vec3f RayDirection = NORMALIZE(PointOnImagePlane - RayOrigin);			//max lenght

	RaycastResult Result = Raycast4(RayOrigin, RayDirection, ToCheck4, FLT_MAX);

	// if we hit something, fetch a color and insert into image
	Vec3f Ei{0,0,0};
	Vec3f Throughput{1, 1, 1};
	int Bounce = 0;
	float Prob = 1;
	while(true)
	{
		{
			DebugLine DL = DebugLine{Result.Origin, Result.Point, Vec3f{0.7f, 0, 0.7f}};
			DebugLines.push_back(DL);
		}

		if (Result.Tri != nullptr)
		{
			Vec3f DiffuseTex;
			Vec3f MappedNormal;
			Vec3f TriangleNormal = Result.Tri->Normal;
			Vec3f Specular;
			Vec3f Point = Result.Point;

			GetTextureParameters(Result, DiffuseTex, MappedNormal, Specular);
			if(DOT(MappedNormal, Result.Dir) > 0)		//flip normal if wrong way
			{
				MappedNormal = -MappedNormal;
				TriangleNormal = -TriangleNormal;
			}

			float pdf;
			Vec3f LightPos;
			AreaLight *AL = GetAreaLight();
			AL->Sample(pdf, LightPos, R);
			Vec3f DirectionToLight = LightPos - Point;
			float r = LEN(DirectionToLight);
			DirectionToLight = DirectionToLight/r;
			RaycastResult ShadowRes = Raycast4(Point+0.001f*MappedNormal, DirectionToLight, ToCheck4, r);

			{
				DebugLine DL;
				if(ShadowRes.Tri == nullptr)
				{
					DL = DebugLine{Point, LightPos, Vec3f{0.7f, 0.7f, 0}};
				}
				else
				{
					DL = DebugLine{Point, ShadowRes.Point, Vec3f{0, 0, 0.7f}};
				}
				DebugLines.push_back(DL);												//shadowray

				DL = DebugLine{Point, Point+MappedNormal, Vec3f{1, 0, 0}};			
				DebugLines.push_back(DL);												//normal

				std::cout << "Diffuse: " << DiffuseTex << " Specular: " << Specular << " Normal: " << MappedNormal << std::endl;

				Vec2f uv = (1 - Result.u - Result.v)*Result.Tri->Vertices[0].T + Result.u*Result.Tri->Vertices[1].T + Result.v*Result.Tri->Vertices[2].T;
				Vec2i tc = GetTexelCoords(uv, {1024,1024});
				std::cout << "UV: " << uv << " Texelcords: " << tc << std::endl;
			}

			//float materailpdf = cos(theta)/PI;
			Throughput *= DiffuseTex + Specular; //  * cos(theta)/PI / materialpdf
			if(ShadowRes.Tri == nullptr)
			{
				// if not, add the appropriate emission, 1/r^2 and clamped cosine terms, accounting for the PDF as well.
				// accumulate into E
				
				Vec3i LEmission = AL->GetEmission();
				Vec3f LNorm = AL->GetNormal();
				float cos1  = DOT(-DirectionToLight, LNorm)/(LEN(LNorm) * r);					//xy and light normal
				float cos2  = DOT(DirectionToLight, MappedNormal)/(LEN(MappedNormal) * r);		//incoming and surface normal
				if(cos1 < 0)
					cos1 = 0;
				if(cos2 < 0)
					cos2 = 0;

				Ei += ((cos1 * cos2)/(r*r * pdf))*LEmission * Prob * Throughput / PI;			//add contribution
			}
			Vec3f Dir = CosDir(MappedNormal, TriangleNormal, 1, R);
			if(SD_->RussianRoulette)						//russion roulette
			{
				if(Bounce < SD_->MaxBounces)		
				{
					++Bounce;						//continue
					Result = Raycast4(Point+Dir*0.001f, Dir, ToCheck4, FLT_MAX);
				}
				else
				{
					float Rand = R.GetF32(0, 1);
					if(Rand < 0.2)
					{
						++Bounce;					//continue
						Prob *= 1/0.8;
						Result = Raycast4(Point+Dir*0.001f, Dir, ToCheck4, FLT_MAX);
					}
					else
					{
						break;			//terminate
					}
				}
			}
			else
			{
				if(Bounce < SD_->MaxBounces)		//normal
				{
					++Bounce;					//continue
					Result = Raycast4(Point+Dir*0.001f, Dir, ToCheck4, FLT_MAX);
				}
				else
				{
					break;			//terminate
				}
			}
		}
		else
		{
			break;		//missed
		}
	}
	return Ei;
}

Vec3f PathTracer::DebugPath(int ImageX, int ImageY, Vec2i ImageSize, std::vector<DebugLine> &DebugLines, Random &R)
{
	std::vector<Node*> ToCheck;
	ToCheck.reserve(BVH_.size());

	float x = (float)ImageX / ImageSize.x * 2.0f - 1.0f;		//from -1 to 1
	float y = (float)ImageY / ImageSize.y * 2.0f - 1.0f;

	Camera *Cam = GetCamera();
	Vec3f RayOrigin = Cam->Pos;
	Vec3f PointOnImagePlane = Cam->Pos + 1*Cam->Dir + x*Cam->Right + y*Cam->Up;
	Vec3f RayDirection = FLT_MAX*NORMALIZE(PointOnImagePlane - RayOrigin);			//max lenght

	RaycastResult Result = Raycast(RayOrigin, RayDirection, ToCheck, 1);

	// if we hit something, fetch a color and insert into image
	Vec3f Ei{0,0,0};
	Vec3f Throughput{1, 1, 1};
	int Bounce = 0;
	float Prob = 1;
	while(true)
	{
		{
			DebugLine DL = DebugLine{Result.Origin, Result.Point, Vec3f{0.7f, 0, 0.7f}};
			DebugLines.push_back(DL);
		}

		if (Result.Tri != nullptr)
		{
			Vec3f DiffuseTex;
			Vec3f MappedNormal;
			Vec3f TriangleNormal = Result.Tri->Normal;
			Vec3f Specular;
			Vec3f Point = Result.Point;

			GetTextureParameters(Result, DiffuseTex, MappedNormal, Specular);
			if(DOT(MappedNormal, Result.Dir) > 0)		//flip normal if wrong way
			{
				MappedNormal = -MappedNormal;
				TriangleNormal = -TriangleNormal;
			}

			float pdf;
			Vec3f LightPos;
			AreaLight *AL = GetAreaLight();
			AL->Sample(pdf, LightPos, R);
			Vec3f DirectionToLight = LightPos - Point;
			RaycastResult ShadowRes = Raycast(Point+0.001f*MappedNormal, DirectionToLight, ToCheck, 1);

			{
				DebugLine DL;
				if(ShadowRes.Tri == nullptr)
				{
					DL = DebugLine{Point, LightPos, Vec3f{0.7f, 0.7f, 0}};
				}
				else
				{
					DL = DebugLine{Point, ShadowRes.Point, Vec3f{0, 0, 0.7f}};
				}
				DebugLines.push_back(DL);												//shadowray

				DL = DebugLine{Point, Point+MappedNormal, Vec3f{1, 0, 0}};			
				DebugLines.push_back(DL);												//normal

				std::cout << "Diffuse: " << DiffuseTex << " Specular: " << Specular << " Normal: " << MappedNormal << std::endl;

				Vec2f uv = (1 - Result.u - Result.v)*Result.Tri->Vertices[0].T + Result.u*Result.Tri->Vertices[1].T + Result.v*Result.Tri->Vertices[2].T;
				Vec2i tc = GetTexelCoords(uv, {1024,1024});
				std::cout << "UV: " << uv << " Texelcords: " << tc << std::endl;
			}

			//float materailpdf = cos(theta)/PI;
			Throughput *= DiffuseTex + Specular; //  * cos(theta)/PI / materialpdf
			if(ShadowRes.Tri == nullptr)
			{
				// if not, add the appropriate emission, 1/r^2 and clamped cosine terms, accounting for the PDF as well.
				// accumulate into E
				float r = LEN(DirectionToLight);
				Vec3i LEmission = AL->GetEmission();
				Vec3f LNorm = AL->GetNormal();
				float cos1  = DOT(-DirectionToLight, LNorm)/(LEN(LNorm) * r);					//xy and light normal
				float cos2  = DOT(DirectionToLight, MappedNormal)/(LEN(MappedNormal) * r);		//incoming and surface normal
				if(cos1 < 0)
					cos1 = 0;
				if(cos2 < 0)
					cos2 = 0;

				Ei += ((cos1 * cos2)/(r*r * pdf))*LEmission * Prob * Throughput / PI;			//add contribution
			}
			Vec3f Dir = CosDir(MappedNormal, TriangleNormal, FLT_MAX, R);
			if(SD_->RussianRoulette)						//russion roulette
			{
				if(Bounce < SD_->MaxBounces)		
				{
					++Bounce;						//continue
					Result = Raycast(Point+Dir*0.001f/FLT_MAX, Dir, ToCheck, 1);
				}
				else
				{
					float Rand = R.GetF32(0, 1);
					if(Rand < 0.2)
					{
						++Bounce;					//continue
						Prob *= 1/0.8;
						Result = Raycast(Point+Dir*0.001f/FLT_MAX, Dir, ToCheck, 1);
					}
					else
					{
						break;			//terminate
					}
				}
			}
			else
			{
				if(Bounce < SD_->MaxBounces)		//normal
				{
					++Bounce;					//continue
					Result = Raycast(Point+Dir*0.001f/FLT_MAX, Dir, ToCheck, 1);
				}
				else
				{
					break;			//terminate
				}
			}
		}
		else
		{
			break;		//missed
		}
	}
	return Ei;
}

AreaLight * PathTracer::GetAreaLight()
{
	return &SD_->AreaLight;
}

PointLight * PathTracer::GetPointLight()
{
	return &SD_->PointLight;
}

Camera * PathTracer::GetCamera()
{
	return &SD_->Cam;
}

void PathTracer::SetCamera(Camera Cam)
{
	SD_->Cam = Cam;
}