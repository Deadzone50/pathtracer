#pragma once
#include <string>
#include <random>
#include <chrono>
#include "Vector/Vector.h"

class Random
{
public:
	Random() {};
	Random(unsigned seed) {Gen.seed(seed);};
	void Seed(unsigned seed) {Gen.seed(seed);}
	Vec2f GetVec2f(float low, float high)
	{
		float f1 = (float)Gen()/Gen.max()*(high-low)+low;
		float f2 = (float)Gen()/Gen.max()*(high-low)+low;
		Vec2f Result{f1, f2};
		return Result;
	}
	float GetF32(float low, float high)
	{
		float Result = (float)Gen()/Gen.max()*(high-low)+low;
		return Result;
	}
	std::mt19937 Gen;
};

class AreaLight
{
public:
	AreaLight(): Emission_{100, 100, 100}, Size_{0.25f, 0.25f} { }

	// this function draws samples on the light source for computing direct illumination
	void Sample( float &pdf, Vec3f &Pos, Random &R);

	Vec3f GetPosition(void) const			{ return Position_; }
	void SetPosition(const Vec3f &P)		{Position_ = P; }

	Vec3f GetNormal(void) const				{ return Direction_; }
	void SetNormal(const Vec3f &D)
	{
		Direction_ = D;
		Right_ = NORMALIZE(CROSS(Direction_, {0,1,0}));
		Up_ =  NORMALIZE(CROSS(Direction_, -Right_));
	}

	Vec2f GetSize(void) const				{ return Size_; }
	void SetSize(const Vec2f &S)			{ Size_ = S; }

	Vec3i GetEmission(void) const			{ return Emission_; }
	void SetEmission(const Vec3i& E)		{ Emission_ = E; }

	//void			draw( const Mat4f& worldToCamera, const Mat4f& projection  );

	//	void			readState( StateDump& d )			{ d.pushOwner("areaLight"); d.get(m_xform,"xform"); d.get(m_size,"size"); d.get(m_E,"E"); d.popOwner(); }
	//	void			writeState( StateDump& d ) const	{ d.pushOwner("areaLight"); d.set(m_xform,"xform"); d.set(m_size,"size"); d.set(m_E,"E"); d.popOwner(); }

	Vec3f	Direction_, Position_, Right_, Up_;
	Vec2f	Size_;		// Physical size of the emitter from the center of the light. I.e. half of the total width/height.
	Vec3i	Emission_;	// Diffuse emission (W/m^2).
};

class PointLight
{
public:
	PointLight(): Emission_{100, 100, 100} { }

	// this function draws samples on the light source for computing direct illumination
	void Sample(Vec3f &Pos)					{ Pos = Position_; }

	Vec3f GetPosition(void) const			{ return Position_; }
	void SetPosition(const Vec3f &P)		{ Position_ = P; }

	Vec3i GetEmission(void) const			{ return Emission_; }
	void SetEmission(const Vec3i& E)		{ Emission_ = E; }

	//void			draw( const Mat4f& worldToCamera, const Mat4f& projection  );

	//	void			readState( StateDump& d )			{ d.pushOwner("areaLight"); d.get(m_xform,"xform"); d.get(m_size,"size"); d.get(m_E,"E"); d.popOwner(); }
	//	void			writeState( StateDump& d ) const	{ d.pushOwner("areaLight"); d.set(m_xform,"xform"); d.set(m_size,"size"); d.set(m_E,"E"); d.popOwner(); }

	Vec3f	Position_;
	Vec3i	Emission_;	// Diffuse emission (W/m^2).
};

struct Camera
{
	Vec2f Angle;			// angle of rotation for the camera direction
	Vec3f Up, Dir, Pos, Right;
};

struct SceneData
{
	Camera Cam;
	AreaLight AreaLight;
	PointLight PointLight;
	int MaxBounces;
	bool RussianRoulette;
};

struct Vertex
{
	Vec3f P, N;
	Vec2f T;
};

struct Texture
{
	int W, H, BPP;
	uint8_t *ImageData;
};

enum TextureMap
{
	//map_Ka,		//ambient map
	map_Kd,			//diffuse map
	map_Ks,			//specular map
	//map_Ns,		//specular exponent map
	map_bump,		//bump/normal map
	map_disp,			//displacement map
	//map_d,		//dissolve/alpha map
	
	MaxTextureMaps	//used for the containers size
};

struct Material
{
	Vec3f	Diffuse;
	Vec3f	Specular;
	Vec3f	Emission;
	float	Alpha;
	float	Glossiness;
	float	IndexOfRefraction;
	Vec3f	TransparencyFilter;
	Vec3f	Reflectivity;

	std::vector<Texture> TextureMaps;
	//float	DisplacementCoef; // height = texture/255 * coef + bias
	//float	DisplacementBias;
	

	//Texture	Textures[TextureType_Max];

	//Material(void)
	//{
	//	Diffuse             = Vec4f(0.75f, 0.75f, 0.75f, 1.0f);
	//	Specular            = 0.5f;
	//	Glossiness          = 32.0f;
	//	DisplacementCoef    = 1.0f;
	//	DisplacementBias    = 0.0f;
	//}
};

class Triangle 
{
public:
	Vec3f Min()			//return smallest x,y,z in a vec3f
	{
		Vec3f Min = {FLT_MAX, FLT_MAX, FLT_MAX};
		Vec3f Val = Vertices[0].P;
		if(Val.x < Min.x)
			Min.x = Val.x;
		if(Val.y < Min.y)
			Min.y = Val.y;
		if(Val.z < Min.z)
			Min.z = Val.z;

		Val = Vertices[1].P;
		if(Val.x < Min.x)
			Min.x = Val.x;
		if(Val.y < Min.y)
			Min.y = Val.y;
		if(Val.z < Min.z)
			Min.z = Val.z;

		Val = Vertices[2].P;
		if(Val.x < Min.x)
			Min.x = Val.x;
		if(Val.y < Min.y)
			Min.y = Val.y;
		if(Val.z < Min.z)
			Min.z = Val.z;

		return Min;
	}
	Vec3f Max()			//return largest x,y,z in a vec3f
	{
		Vec3f Max = {-FLT_MAX, -FLT_MAX, -FLT_MAX};
		Vec3f Val = Vertices[0].P;
		if(Val.x > Max.x)
			Max.x = Val.x;
		if(Val.y > Max.y)
			Max.y = Val.y;
		if(Val.z > Max.z)
			Max.z = Val.z;

		Val = Vertices[1].P;
		if(Val.x > Max.x)
			Max.x = Val.x;
		if(Val.y > Max.y)
			Max.y = Val.y;
		if(Val.z > Max.z)
			Max.z = Val.z;

		Val = Vertices[2].P;
		if(Val.x > Max.x)
			Max.x = Val.x;
		if(Val.y > Max.y)
			Max.y = Val.y;
		if(Val.z > Max.z)
			Max.z = Val.z;

		return Max;
	}
	inline Vec3f Center() const
	{
		return (1.0f / 3.0f)*(Vertices[0].P + Vertices[1].P + Vertices[2].P);
	}
	bool Intersect(const Vec3f &Origin, const Vec3f &Dir, float &t, float &u, float &v);

	Vertex Vertices[3];
	Vec3f Normal;
	Material *Mat;
};