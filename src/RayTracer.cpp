#include <iostream>

#include "PathTracer.h"

Vec3f MirrorDir(Vec3f Dir, Vec3f N)
{
	Vec3f Mirror = Dir - 2*DOT(Dir, N)*N;
	return Mirror;
}

bool
PathTracer::RayRender(Vec2i ImageSize, std::vector<ImageVec3> &ImageBuffer, int SectionSize, int CurrentSection)
{
	//std::vector<DebugLine> DBL;
	const int Start = CurrentSection*SectionSize;			//imagesize must be evenly dividable by sectionsize
	const int StartX = Start % ImageSize.x;
	const int StartY = (Start / ImageSize.x)*SectionSize;

	if(StartY >= ImageSize.y)				//outside the image, we are done
		return true;

	int y = StartY;
	int x = StartX;

	int EndX, EndY;							//choose either end of section or image
	if(StartY+SectionSize < ImageSize.y)
		EndY = StartY+SectionSize;
	else
		EndY = ImageSize.y;

	if(StartX+SectionSize < ImageSize.x)
		EndX = StartX+SectionSize;
	else
		EndX = ImageSize.x;

	Camera *Cam = GetCamera();
	Vec3f RayOrigin = Cam->Pos;

	#pragma omp parallel					//create threads
	{
		std::vector<Node*> ToCheck;
		ToCheck.reserve(BVH_.size());

		#pragma omp for						//split for loop for threads
		for(y = StartY; y < EndY; ++y)
		{
			float Y = (float)y / ImageSize.y * 2.0f - 1.0f;
			for(x = StartX; x < EndX; ++x)
			{
				// Generate a ray through the pixel.
				// NOTE that it's not normalized; the direction is defined
				// so that the segment to be traced is [Ro, Ro+Rd]

				float X = (float)x / ImageSize.x * 2.0f - 1.0f;		//from -1 to 1

				Vec3f PointOnImagePlane = Cam->Pos + 1*Cam->Dir + X*Cam->Right + Y*Cam->Up;
				Vec3f RayDirection = FLT_MAX*NORMALIZE(PointOnImagePlane - RayOrigin);			//max lenght

				Vec3f Color = RayTrace(RayOrigin, RayDirection, ToCheck);
				ImageBuffer[ImageSize.x*y + x].Color += Color;
				ImageBuffer[ImageSize.x*y + x].Samples += 1;
			}
		}
	}
	return false;
}

Vec3f PathTracer::DebugRay(int ImageX, int ImageY, Vec2i ImageSize, std::vector<DebugLine> &DebugLines)
{
	std::vector<Node*> ToCheck;
	ToCheck.reserve(BVH_.size());

	// Generate a ray through the pixel.
	// NOTE that it's not normalized; the direction is defined
	// so that the segment to be traced is [Ro, Ro+Rd]

	float x = (float)ImageX / ImageSize.x * 2.0f - 1.0f;		//from -1 to 1
	float y = (float)ImageY / ImageSize.y * 2.0f - 1.0f;

	Camera *Cam = GetCamera();
	Vec3f RayOrigin = Cam->Pos;
	Vec3f PointOnImagePlane = Cam->Pos + 1*Cam->Dir + x*Cam->Right + y*Cam->Up;
	Vec3f RayDirection = FLT_MAX*NORMALIZE(PointOnImagePlane - RayOrigin);			//max lenght

	RaycastResult Result = Raycast(RayOrigin, RayDirection, ToCheck, 1);			// trace!

	Vec3f Ei{0,0,0};
	Vec3f Throughput{1, 1, 1};
	int CurrentBounce = 0;
	while(true)
	{
		//if(Debug)				//Ray
		{
			DebugLine DL = DebugLine{Result.Origin, Result.Point, Vec3f{0.7f, 0, 0.7f}};
			DebugLines.push_back(DL);
		}
		if (Result.Tri != nullptr)
		{
			Vec3f DiffuseTex;
			Vec3f MappedNormal;
			Vec3f TriangleNormal = Result.Tri->Normal;
			Vec3f Specular;
			Vec3f Point = Result.Point;
			GetTextureParameters(Result, DiffuseTex, MappedNormal, Specular);
			if(DOT(MappedNormal, Result.Dir) > 0)		//flip normal if wrong way
			{
				MappedNormal = -MappedNormal;
				TriangleNormal = -TriangleNormal;
			}

			Vec3f LightPos;
			PointLight *PL = GetPointLight();
			PL->Sample(LightPos);
			Vec3f DirectionToLight = LightPos - Point;
			RaycastResult ShadowRes = Raycast(Point+0.001f*MappedNormal, DirectionToLight, ToCheck, 1);		//check for objects between here and light

																											//if(Debug)				//Shadowray
			{
				DebugLine DL;
				if(ShadowRes.Tri == nullptr)
				{
					DL = DebugLine{Point, LightPos, Vec3f{0.7f, 0.7f, 0}};
				}
				else
				{
					DL = DebugLine{Point, ShadowRes.Point, Vec3f{0, 0, 0.7f}};
				}
				DebugLines.push_back(DL);												//shadowray

				DL = DebugLine{Point, Point+MappedNormal, Vec3f{1, 0, 0}};			
				DebugLines.push_back(DL);												//normal

				std::cout << "Diffuse: " << DiffuseTex << " Specular: " << Specular << " Normal: " << MappedNormal << std::endl;
			}

			Vec3f V = NORMALIZE(Result.Origin - Point);					//direction toward "camera"
			Vec3f PSpecular = Vec3f{0,0,0};								//phong shading
			Vec3f PDiffuse = Vec3f{0,0,0};
			Vec3f PAmbient = Vec3f{0.05f, 0.05f, 0.05f}*DiffuseTex;
			if(ShadowRes.Tri == nullptr)
			{
				float r = LEN(DirectionToLight);
				if(r < 1)			//clamp
					r = 1;
				Vec3f LightColor = PL->GetEmission()/(r*r);
				Vec3f L = DirectionToLight/r;							//direction toward light

				float d = DOT(L, MappedNormal);							//diffuse
				PDiffuse += d*DiffuseTex*LightColor;

				Vec3f R = MirrorDir(-L, MappedNormal);					//specular
				float s = DOT(R, V);
				if(s > 0)
				{
					s = powf(s, Result.Tri->Mat->Glossiness);
					PSpecular += s*Specular*LightColor;
				}
			}
			Ei += PAmbient + Throughput*(PDiffuse + PSpecular);
			if(CurrentBounce < SD_->MaxBounces)
			{
				++CurrentBounce;
			}
			else
			{
				break;			//no bounces left
			}

			bool Continue = false;
			Material *Mat = Result.Tri->Mat;
			Vec3f Dir;
			if(LEN(Mat->Reflectivity))
			{
				Continue = true;
				Dir = MirrorDir(-V, MappedNormal)*FLT_MAX;
				Throughput *= Mat->Specular*0.1f;
			}
			if(LEN(Mat->TransparencyFilter))
			{
				Continue = true;
				//Dir = snells law
			}

			if(Continue)
			{
				Result = Raycast(Point+Dir*0.001f/FLT_MAX, Dir, ToCheck, 1);
			}
			else
			{
				break;				//not reflective or transparent
			}
		}
		else
		{
			break;					//missed
		}
	}
	return Ei;
}

Vec3f
PathTracer::RayTrace(Vec3f RayOrigin, Vec3f RayDirection, std::vector<Node*> &ToCheck)
{
	RaycastResult Result = Raycast(RayOrigin, RayDirection, ToCheck, 1);				// trace!

	Vec3f Ei{0,0,0};
	Vec3f Throughput{1, 1, 1};
	int CurrentBounce = 0;
	while(true)
	{
		if (Result.Tri != nullptr)
		{
			Vec3f DiffuseTex;
			Vec3f MappedNormal;
			Vec3f TriangleNormal = Result.Tri->Normal;
			Vec3f Specular;
			Vec3f Point = Result.Point;
			GetTextureParameters(Result, DiffuseTex, MappedNormal, Specular);
			if(DOT(MappedNormal, Result.Dir) > 0)		//flip normal if wrong way
			{
				MappedNormal = -MappedNormal;
				TriangleNormal = -TriangleNormal;
			}

			Vec3f LightPos;
			PointLight *PL = GetPointLight();
			PL->Sample(LightPos);
			Vec3f DirectionToLight = LightPos - Point;		//check for objects between here and light
			RaycastResult ShadowRes = Raycast(Point+0.001f*MappedNormal, DirectionToLight, ToCheck, 1);

			Vec3f V = NORMALIZE(Result.Origin - Point);					//direction toward "camera"
			Vec3f PSpecular = Vec3f{0,0,0};								//phong shading
			Vec3f PDiffuse = Vec3f{0,0,0};
			Vec3f PAmbient = Vec3f{0.05f, 0.05f, 0.05f}*DiffuseTex;
			if(ShadowRes.Tri == nullptr)
			{
				float r = LEN(DirectionToLight);
				if(r < 1)			//clamp
					r = 1;
				Vec3f LightColor = PL->GetEmission()/(r*r);
				Vec3f L = DirectionToLight/r;							//direction toward light

				float d = DOT(L, MappedNormal);							//diffuse
				PDiffuse += d*DiffuseTex*LightColor;

				Vec3f R = MirrorDir(-L, MappedNormal);					//specular
				float s = DOT(R, V);
				if(s > 0)
				{
					s = powf(s, Result.Tri->Mat->Glossiness);
					PSpecular += s*Specular*LightColor;
				}
			}
			Ei += PAmbient + Throughput*(PDiffuse + PSpecular);
			if(CurrentBounce < SD_->MaxBounces)
			{
				++CurrentBounce;
			}
			else
			{
				break;			//no bounces left
			}

			bool Continue = false;
			Material *Mat = Result.Tri->Mat;
			Vec3f Dir;
			if(LEN(Mat->Reflectivity))
			{
				Continue = true;
				Dir = MirrorDir(-V, MappedNormal)*FLT_MAX;
				Throughput *= Mat->Specular*0.1f;
			}
			if(LEN(Mat->TransparencyFilter))
			{
				Continue = true;
				//Dir = snells law
			}

			if(Continue)
			{
				Result = Raycast(Point+Dir*0.001f/FLT_MAX, Dir, ToCheck, 1);
			}
			else
			{
				break;				//not reflective or transparent
			}
		}
		else
		{
			break;					//missed
		}
	}
	return Ei;
}
