#include <GL/glut.h>
#include <chrono>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include "Vector/Vector.h"
#include "Vector/Matrix.h"
#include "Scene.h"
#include "PathTracer.h"
#include "Sidebar.h"
bool* KeyStates = new bool[256]{false};
bool* KeySpecialStates = new bool[256]{false};

SceneData SD{0};

std::string CurrentScene;

Vec2i ScreenSize;
int SideBarWidth, TopBarHeight;
Sidebar SB;

std::vector<ImageVec3> ImageBuffer;
PathTracer PT;

inline void glColor3f(Vec3f V)
{
	glColor3f(V.x, V.y, V.z);
}
inline void glVertex3f(Vec3f V)
{
	glVertex3f(V.x, V.y, V.z);
}

void KeyDown(unsigned char Key, int MouseX, int MouseY)
{
	KeyStates[Key] = true;
}

void KeyUp(unsigned char Key, int MouseX, int MouseY)
{
	KeyStates[Key] = false;
	if('a' <= Key && Key <= 'z')
	{
		KeyStates[Key-32] = false;
	}
	if('A' <= Key && Key <= 'Z')
	{
		KeyStates[Key+32] = false;
	}

}

void KeySpecialDown(int Key, int MouseX, int MouseY)
{
	KeySpecialStates[Key] = true;
}

void KeySpecialUp(int Key, int MouseX, int MouseY)
{
	KeySpecialStates[Key] = false;
}

int SectionSize;
int CurrentSection = 0;
bool RedrawWireframe = true;
bool NewRender = false;

Vec3f PointToDraw;
Vec2i PointPos;
std::vector<DebugLine> DebugLines;
bool DebugDraw = false;

#define GLUT_MOUSEWHEEL_UP 3
#define GLUT_MOUSEWHEEL_DOWN 4

float Speed = 0.1f;

void MouseFunc(int button, int state, int x, int Y)
{
	int y = ScreenSize.y+TopBarHeight -Y;
	if(button == GLUT_LEFT_BUTTON)
	{
		if(state == GLUT_DOWN)
		{
			if(x < ScreenSize.x)			//main area
			{
				DebugLines.clear();
				unsigned seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
				PointToDraw = PT.DebugPath4(x, y, ScreenSize, DebugLines, Random(seed));
				PointPos.x = x;
				PointPos.y = y;
				DebugDraw = true;
				RedrawWireframe = true;
			}
			else							//sidebar
			{
				float xx = (float)(x-ScreenSize.x) /SideBarWidth * 2.0f - 1.0f;
				float yy = (float)y /(ScreenSize.y+TopBarHeight) * 2.0f - 1.0f;
				auto MEND = SB.MenuButtons.end();
				for(auto it = SB.MenuButtons.begin(); it != MEND; ++it)
				{

					std::string Response = it->Click(xx, yy);
					if(Response == "miss")
					{
						continue;
					}
					else if(Response == "Light+")
					{
						Vec3i ls = SD.PointLight.GetEmission();
						if(glutGetModifiers() == GLUT_ACTIVE_SHIFT)
						{
							ls += {10,10,10};
						}
						else
							ls += {1,1,1};
						SD.AreaLight.SetEmission(ls);
						SD.PointLight.SetEmission(ls);
						RedrawWireframe = true;
					}
					else if(Response == "Light-")
					{
						Vec3i ls = SD.PointLight.GetEmission();
						if(glutGetModifiers() == GLUT_ACTIVE_SHIFT)
						{
							ls -= {10,10,10};
						}
						else
							ls -= {1,1,1};
						SD.AreaLight.SetEmission(ls);
						SD.PointLight.SetEmission(ls);
						RedrawWireframe = true;
					}
					else if(Response == "Bounces+")
					{
						++SD.MaxBounces;
						RedrawWireframe = true;
					}
					else if(Response == "Bounces-")
					{
						--SD.MaxBounces;
						RedrawWireframe = true;
					}
					else if(Response == "Save scene")
					{
						PT.SaveScene(CurrentScene+".scene");
					}
				}
			}
		}
	}
	if(button == GLUT_RIGHT_BUTTON)
	{
		if(state == GLUT_DOWN)
		{
			if(x < ScreenSize.x)
			{
				Vec2f Turn;
				Turn.x = (float)x/ScreenSize.x * 2 - 1;
				Turn.y = (float)y/ScreenSize.y * 2 - 1;
				Turn = 0.5f*Turn;

				Vec3f R = CROSS(SD.Cam.Dir, Vec3f{0,1,0});				//calculate new direction of camera, R=right along x-z plane
				SD.Cam.Angle += Turn;
				Mat3f M;
				M.Rotate({-SD.Cam.Angle.y, SD.Cam.Angle.x, 0});
				SD.Cam.Dir = M*Vec3f{0,0,-1};
				SD.Cam.Up = NORMALIZE(CROSS(R, SD.Cam.Dir));
				SD.Cam.Right = NORMALIZE(CROSS(SD.Cam.Dir, SD.Cam.Up));

				RedrawWireframe = true;
			}
		}
	}
	if(button == GLUT_MOUSEWHEEL_UP)
	{
		Speed *= 2;
	}
	if(button == GLUT_MOUSEWHEEL_DOWN)
	{
		Speed /= 2;
	}
}

bool Path = false;
bool Path4 = false;
bool Ray = false;

auto TraceBegin = std::chrono::high_resolution_clock::now();

void KeyOperations(void)
{
	
	if(KeyStates['q'])
	{
		exit(EXIT_SUCCESS);
	}
	
	float fraction = 0.1f;
	float turbo = 1.0f;
	Vec2f Turn = {0};
	if(KeyStates['a'])
	{
		SD.Cam.Pos -= Speed * SD.Cam.Right;
		RedrawWireframe = true;
	}
	if(KeyStates['d'])
	{
		SD.Cam.Pos += Speed * SD.Cam.Right;
		RedrawWireframe = true;
	}
	if(KeyStates['w'])
	{
		SD.Cam.Pos += Speed * SD.Cam.Dir;
		RedrawWireframe = true;
	}
	if(KeyStates['s'])
	{
		SD.Cam.Pos -= Speed * SD.Cam.Dir;
		RedrawWireframe = true;
	}
	if(KeyStates['e'])
	{
		CurrentSection = 0;
		//ImageBuffer.clear();
		//ImageBuffer.resize(ScreenSize.x*ScreenSize.y);
		Ray = false;
		Path = true;
		Path4 = false;
		TraceBegin = std::chrono::high_resolution_clock::now();
		PT.RayCount = 0;
	}
	if(KeyStates['t'])
	{
		CurrentSection = 0;
		//ImageBuffer.clear();
		//ImageBuffer.resize(ScreenSize.x*ScreenSize.y);
		Ray = false;
		Path = false;
		Path4 = true;
		TraceBegin = std::chrono::high_resolution_clock::now();
		PT.RayCount = 0;
	}
	if(KeyStates['r'])
	{
		CurrentSection = 0;
		ImageBuffer.clear();
		ImageBuffer.resize(ScreenSize.x*ScreenSize.y);
		Ray = true;
		Path = false;
		Path4 = false;
		TraceBegin = std::chrono::high_resolution_clock::now();
		PT.RayCount = 0;
	}
	if(KeyStates['E'] || KeyStates['T'] || KeyStates['R'])
	{
		Path = false;
		Path4 = false;
		Ray = false;
	}
	if(KeyStates['1'])
	{
		CurrentScene = "cornell";
		PT.OpenScene("Scenes/"+CurrentScene+"/", CurrentScene, ".obj");
		RedrawWireframe = true;
		Path = false;
		Ray = false;
	}
	if(KeyStates['2'])
	{
		CurrentScene = "conference";
		PT.OpenScene("Scenes/"+CurrentScene+"/", CurrentScene, ".obj");
		RedrawWireframe = true;
		Path = false;
		Ray = false;
	}
	if(KeyStates['3'])
	{
		CurrentScene = "sponza";
		PT.OpenScene("Scenes/"+CurrentScene+"/", CurrentScene, ".obj");
		RedrawWireframe = true;
		Path = false;
		Ray = false;
	}
	if(KeyStates['4'])
	{
		CurrentScene = "Crysponza";
		PT.OpenScene("Scenes/"+CurrentScene+"/", CurrentScene, ".obj");
		RedrawWireframe = true;
		Path = false;
		Ray = false;
	}
	if(KeyStates['5'])
	{
		CurrentScene = "cornell_chesterfield";
		PT.OpenScene("Scenes/"+CurrentScene+"/", CurrentScene, ".obj");
		RedrawWireframe = true;
		Path = false;
		Ray = false;
	}
	if(KeyStates[' '])
	{
		SD.AreaLight.SetPosition(SD.Cam.Pos);
		SD.AreaLight.SetNormal(SD.Cam.Dir);
		SD.PointLight.SetPosition(SD.Cam.Pos);
		std::cout << "set light position to: " << SD.Cam.Pos << std::endl;
		RedrawWireframe = true;
	}

	if(Path)
	{
		if(PT.PathRender(ScreenSize, ImageBuffer, SectionSize, CurrentSection))
		{
			CurrentSection = 0;
		}
		else
			++CurrentSection;
		NewRender = true;
	}
	else if(Path4)
	{
		if(PT.PathRender4(ScreenSize, ImageBuffer, SectionSize, CurrentSection))
		{
			CurrentSection = 0;
		}
		else
			++CurrentSection;
		NewRender = true;
	}
	if(Ray)
	{
		if(PT.RayRender(ScreenSize, ImageBuffer, SectionSize, CurrentSection))
		{
			CurrentSection = 0;
			Ray = false;
		}
		else
			++CurrentSection;
		NewRender = true;
	}

	if(RedrawWireframe)
	{
		Vec3f R = CROSS(SD.Cam.Dir, Vec3f{0,1,0});				//calculate new direction of camera, R=right along x-z plane
		SD.Cam.Angle += Turn;
		Mat3f M;
		M.Rotate({-SD.Cam.Angle.y, SD.Cam.Angle.x, 0});
		SD.Cam.Dir = M*Vec3f{0,0,-1};
		SD.Cam.Up = NORMALIZE(CROSS(R, SD.Cam.Dir));
		SD.Cam.Right = NORMALIZE(CROSS(SD.Cam.Dir, SD.Cam.Up));
	}
}
void Set3DMode()												//drawing 3d
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90, GLfloat(ScreenSize.x)/GLfloat(ScreenSize.y), 0.1f, 1000.0f);
	//				FOV, Aspect ratio, near-, far-field
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
void Set2DMode()												//drawing text
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1.01f, 1.01f, -1.01f, 1.01f, -1.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void reshape(int width, int height)
{
	ScreenSize.x = width-SideBarWidth;
	ScreenSize.y = height-TopBarHeight;

	ImageBuffer.clear();
	ImageBuffer.resize(ScreenSize.x*ScreenSize.y);

	RedrawWireframe = true;
	SectionSize = ScreenSize.x/10;
}
auto TimeBegin = std::chrono::high_resolution_clock::now();
void display()
{
	TimeBegin = std::chrono::high_resolution_clock::now();
	KeyOperations();

	if(RedrawWireframe)							//draw wireframe
	{
		glViewport(0, 0, GLsizei(ScreenSize.x), GLsizei(ScreenSize.y));

		CurrentSection = 0;
		ImageBuffer.clear();
		ImageBuffer.resize(ScreenSize.x*ScreenSize.y);

		Set3DMode();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		gluLookAt(SD.Cam.Pos.x,					SD.Cam.Pos.y,				SD.Cam.Pos.z,				// Set the camera
				  SD.Cam.Pos.x+SD.Cam.Dir.x,	SD.Cam.Pos.y+SD.Cam.Dir.y,	SD.Cam.Pos.z+SD.Cam.Dir.z,
				  SD.Cam.Up.x,					SD.Cam.Up.y,				SD.Cam.Up.z);
		
		for(auto it = PT.Triangles_.begin(); it != PT.Triangles_.end(); ++it)		//draw wireframe
		{
			glBegin(GL_LINE_LOOP); //Begin triangle coordinates
			glColor3f(it->Mat->Diffuse.x, it->Mat->Diffuse.y, it->Mat->Diffuse.z);
			glVertex3f(it->Vertices[0].P);
			glVertex3f(it->Vertices[1].P);
			glVertex3f(it->Vertices[2].P);
			glEnd(); //End triangle coordinates
		}

		Vec3f LPos = SD.AreaLight.GetPosition();
		Vec3f LDir = SD.AreaLight.GetNormal();
		Vec2f LSize = SD.AreaLight.GetSize();
		Vec3f LRight = CROSS(LDir, {0,1,0});
		Vec3f LUp = CROSS(LRight, LDir);

		Vec3f p1 = LPos + LSize.x*LRight + LSize.y*LUp;
		Vec3f p2 = LPos + LSize.x*LRight - LSize.y*LUp;
		Vec3f p3 = LPos - LSize.x*LRight - LSize.y*LUp;
		Vec3f p4 = LPos - LSize.x*LRight + LSize.y*LUp;
		
		glColor3f(1,1,1);
		glBegin(GL_POLYGON);		//draw area light
		glVertex3f(p1.x, p1.y, p1.z);
		glVertex3f(p2.x, p2.y, p2.z);
		glVertex3f(p3.x, p3.y, p3.z);
		glVertex3f(p4.x, p4.y, p4.z);
		glEnd();

		if(DebugDraw)
		{
			glBegin(GL_LINES);
			for(auto it = DebugLines.begin(); it != DebugLines.end(); ++it)
			{
				glColor3f(it->Color);
				glVertex3f(it->v1+Vec3f{0.001f,0.001f,0.001f});
				glVertex3f(it->v2);
			}
			glEnd();
		}
	}
	else if(NewRender)
	{
#if 1
		glViewport(0, 0, GLsizei(ScreenSize.x), GLsizei(ScreenSize.y));
		Set2DMode();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glPointSize(1);
		glBegin(GL_POINTS);
		for(int y = 0; y < ScreenSize.y; ++y)
		{
			float Y = (float)y/ScreenSize.y * 2 - 1;
			for(int x = 0; x < ScreenSize.x; ++x)
			{
				float X = (float)x/ScreenSize.x * 2 - 1;
				Vec3f Color = ImageBuffer[ScreenSize.x*y + x].Color / (float)ImageBuffer[ScreenSize.x*y + x].Samples;
				glColor3f(Color);
				glVertex2f(X, Y);
			}
		}

		glEnd();
#endif
	}
	if(NewRender || RedrawWireframe)					//upper text & sidebar
	{
#if 1

		auto TimeEnd = std::chrono::high_resolution_clock::now();
		float dt = std::chrono::duration<float>(TimeEnd - TimeBegin).count();
		float dt2 = std::chrono::duration<float>(TimeEnd - TraceBegin).count();

		std::stringstream ss, ss2;
		ss << dt*1000 << " ms";
		ss2 << " pos: " << SD.Cam.Pos << " dir: "<< SD.Cam.Dir << std::endl;

		ss2 << "Rays per second: " << PT.RayCount/(dt2*1000) << " K";
		//PT.RayCount = 0;
		std::string str = ss.str() + ss2.str();

		Set2DMode();
		glViewport(0, GLsizei(ScreenSize.y), GLsizei(ScreenSize.x), GLsizei(TopBarHeight));		//upper text

		glColor3f(0.3f,0.3f,0.3f);
		glBegin(GL_POLYGON);
		glVertex3f(-1.1f, -1.1f, 0);
		glVertex3f(1.1f, -1.1f, 0);
		glVertex3f(1.1f, 1.1f, 0);
		glVertex3f(-1.1f, 1.1f, 0);
		glEnd();

		glColor3f(1,1,1);
		glRasterPos2f(-1.0f, 0);
		for(size_t i = 0; i < str.size();++i)
		{
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, str[i]);
		}

		if(!NewRender)
		{
			glViewport(ScreenSize.x, 0, GLsizei(SideBarWidth), GLsizei(ScreenSize.y+TopBarHeight));		//render in the sidebar
			SB.DrawSidebar(ScreenSize, SideBarWidth);
		}
#endif
		glutSwapBuffers(); //Send to the screen
		RedrawWireframe = false;
		NewRender = false;
	}
}

int main(int argc, char **argv)
{
	ScreenSize.x = 500;
	ScreenSize.y = 500;
	SideBarWidth = 200;
	TopBarHeight = 20;

	ImageBuffer.resize(ScreenSize.x*ScreenSize.y);

	PT.SD_ = &SD;

	SD.Cam.Up = {0,1,0};
	SD.Cam.Dir = {0,0,-1};
	SD.Cam.Pos = {0,0,0};
	SD.Cam.Right = NORMALIZE(CROSS(SD.Cam.Dir, SD.Cam.Up));

	SD.MaxBounces = 2;

	SD.AreaLight.SetPosition({0,0,0});
	SD.AreaLight.SetNormal({0,0,-1});
	SD.AreaLight.SetEmission({1,1,1});

	SD.PointLight.SetPosition({0,0,0});
	SD.PointLight.SetEmission({1,1,1});


	CurrentScene = "cornell";
	//CurrentScene = "conference";
	PT.OpenScene("Scenes/"+CurrentScene+"/", CurrentScene, ".obj");

#if 0
	{
		std::cout << "Raytracing scene\n";
		auto TimeBegin = std::chrono::high_resolution_clock::now();
		PT.RayRender(ScreenSize, ImageBuffer, 500, 0);
		auto TimeEnd = std::chrono::high_resolution_clock::now();
		float dt = std::chrono::duration<float>(TimeEnd - TimeBegin).count();
		std::cout << "Done in " << dt << "seconds" << std::endl;
		std::cout << "Rays per second: " << PT.RayCount/(dt*1000) << " K";
		std::cin.ignore();
		exit(EXIT_SUCCESS);
	}
#endif


	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(ScreenSize.x+SideBarWidth, ScreenSize.y+TopBarHeight);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("PathTracer");

	glutDisplayFunc(display);
	glutIdleFunc(display);
	glutReshapeFunc(reshape);

	glutKeyboardFunc(KeyDown);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(KeySpecialDown);
	glutSpecialUpFunc(KeySpecialUp);
	glutMouseFunc(MouseFunc);

	//glEnable(GL_DEPTH_TEST);

	SB.CreateSidebar(SD);

	glutMainLoop();
}