#include <fstream>
#include <iostream>
#include <deque>

#include "PathTracer.h"
#include "BVH.h"

void ReadNode(std::ifstream &file, Node* N)
{
	Read(file, N->AABB[0].x);
	Read(file, N->AABB[0].y);
	Read(file, N->AABB[0].z);

	Read(file, N->AABB[1].x);
	Read(file, N->AABB[1].y);
	Read(file, N->AABB[1].z);

	Read(file, N->I1);
	Read(file, N->I2);
}

void WriteNode(std::ofstream &file, Node* N)
{
	Write(file, N->AABB[0].x);
	Write(file, N->AABB[0].y);
	Write(file, N->AABB[0].z);

	Write(file, N->AABB[1].x);
	Write(file, N->AABB[1].y);
	Write(file, N->AABB[1].z);

	Write(file, N->I1);
	Write(file, N->I2);
}

void
PathTracer::LoadBVH(std::string Filename)
{
	size_t TrigIsize, BVHsize;
	bool Children;
	std::ifstream file(Filename, std::ios::binary);
	Read(file, TrigIsize);
	TrigI.resize(TrigIsize);
	for(int i = 0; i < TrigIsize; ++i)
	{
		Read(file, TrigI[i]);
	}
	Read(file, BVHsize);

	Node* N = new Node;
	Node* LC;
	Node* RC;
	ReadNode(file, N);
	std::deque<Node*> ToRead;
	ToRead.push_back(N);
	BVH_.push_back(N);

	while(ToRead.size() != 0)
	{
		N = ToRead.front();
		ToRead.pop_front();

		Read(file, Children);		//if it has children

		if(Children)
		{
			LC = new Node;
			ReadNode(file, LC);
			ToRead.push_back(LC);
			BVH_.push_back(LC);
			N->LeftChild = LC;

			RC = new Node;
			ReadNode(file, RC);
			ToRead.push_back(RC);
			BVH_.push_back(RC);
			N->RightChild = RC;
		}
		//else
		//{
		//	N->LeftChild = NULL;
		//	N->RightChild = NULL;
		//}
	}
	file.close();
}

void
PathTracer::SaveBVH(std::string Filename)
{
	std::ofstream file(Filename, std::ios::binary);
	Write(file, TrigI.size());
	for(int i = 0; i < TrigI.size(); ++i)
	{
		Write(file, TrigI[i]);
	}
	Write(file, BVH_.size());

	Node* N = BVH_[0];
	WriteNode(file, N);
	std::deque<Node*> ToWrite;
	ToWrite.push_back(N);

	while(ToWrite.size() != 0)
	{
		N = ToWrite.front();
		ToWrite.pop_front();
		if(N->LeftChild != NULL)
		{
			Write(file, (bool)1);		//if it has children
			WriteNode(file, N->LeftChild);
			ToWrite.push_back(N->LeftChild);

			WriteNode(file, N->RightChild);
			ToWrite.push_back(N->RightChild);
		}
		else
			Write(file, (bool)0);
	}
	file.close();
}

#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

Hit
IntersectsNode(const Node &N, const Vec3f &Origin, const Vec3f &InvDir, Vec3i &Swap, float TMax)		//test if ray hits a node
{
	float T1 = (N.AABB[Swap.V[0]].x - Origin.x)*InvDir.x;		//intersection x	//Ro + Rd*t ,t[0,Tmax]
	float T1Y = (N.AABB[Swap.V[1]].y - Origin.y)*InvDir.y;
	float T1Z = (N.AABB[Swap.V[2]].z - Origin.z)*InvDir.z;

	float T2 = (N.AABB[1-Swap.V[0]].x - Origin.x)*InvDir.x;
	float T2Y = (N.AABB[1-Swap.V[1]].y - Origin.y)*InvDir.y;
	float T2Z = (N.AABB[1-Swap.V[2]].z - Origin.z)*InvDir.z;

	T1 = MAX(T1, T1Y);
	T1 = MAX(T1, T1Z);
	if(T1 > TMax)				//check that it isn't too far
		return Hit{false};
	T2 = MIN(T2, T2Y);
	T2 = MIN(T2, T2Z);

	if(T1 > T2)				//miss
	{
		return Hit{false};
	}
	else if(T2 < 0)			//behind, miss
	{
		return Hit{false};
	}
	else if(T1 > 0)			//hit
	{
		return Hit{true, T1, T2};
	}
	else
	{							//origin inside box
		return Hit{true, 0, T2};
	}
}

Hit4		//test if ray hits a nodes children
IntersectsNode4(const Node4 &N4, const Vec3f &Origin, const Vec3f &InvDir, Vec3i &Swap, float TMax)
{
	Vec3f BMIN[4];
	BMIN[0].x = N4.Child1->AABB[Swap.x].x;
	BMIN[0].y = N4.Child1->AABB[Swap.y].y;
	BMIN[0].z = N4.Child1->AABB[Swap.z].z;

	BMIN[1].x = N4.Child2->AABB[Swap.x].x;
	BMIN[1].y = N4.Child2->AABB[Swap.y].y;
	BMIN[1].z = N4.Child2->AABB[Swap.z].z;

	BMIN[2].x = N4.Child3->AABB[Swap.x].x;
	BMIN[2].y = N4.Child3->AABB[Swap.y].y;
	BMIN[2].z = N4.Child3->AABB[Swap.z].z;

	BMIN[3].x = N4.Child4->AABB[Swap.x].x;
	BMIN[3].y = N4.Child4->AABB[Swap.y].y;
	BMIN[3].z = N4.Child4->AABB[Swap.z].z;

	Vec3f BMAX[4];
	BMAX[0].x = N4.Child1->AABB[1-Swap.x].x;
	BMAX[0].y = N4.Child1->AABB[1-Swap.y].y;
	BMAX[0].z = N4.Child1->AABB[1-Swap.z].z;

	BMAX[1].x = N4.Child2->AABB[1-Swap.x].x;
	BMAX[1].y = N4.Child2->AABB[1-Swap.y].y;
	BMAX[1].z = N4.Child2->AABB[1-Swap.z].z;

	BMAX[2].x = N4.Child3->AABB[1-Swap.x].x;
	BMAX[2].y = N4.Child3->AABB[1-Swap.y].y;
	BMAX[2].z = N4.Child3->AABB[1-Swap.z].z;

	BMAX[3].x = N4.Child4->AABB[1-Swap.x].x;
	BMAX[3].y = N4.Child4->AABB[1-Swap.y].y;
	BMAX[3].z = N4.Child4->AABB[1-Swap.z].z;

	Hit4 H;
	float T1[4], T2[4], T1Y[4], T2Y[4], T1Z[4], T2Z[4];
	for(int i = 0; i < 4; ++i)												//SIMD
	{
		T1[i] = (BMIN[i].x - Origin.x)*InvDir.x;		//intersection x	//Ro + Rd*t ,t[0,Tmax]
		T2[i] = (BMAX[i].x - Origin.x)*InvDir.x;

		T1Y[i] = (BMIN[i].y - Origin.y)*InvDir.y;		//intersection y
		T2Y[i] = (BMAX[i].y - Origin.y)*InvDir.y;

		T1Z[i] = (BMIN[i].z - Origin.z)*InvDir.z;		//intersection z
		T2Z[i] = (BMAX[i].z - Origin.z)*InvDir.z;

		T1[i] = MAX(T1[i], T1Y[i]);
		T1[i] = MAX(T1[i], T1Z[i]);

		T2[i] = MIN(T2[i], T2Y[i]);
		T2[i] = MIN(T2[i], T2Z[i]);
	}
	for(int i = 0; i < 4; ++i)
	{
		if(T1[i] > TMax)				//check that it isn't too far
		{
			H.ChildH[i] = false;
		}
		else if(T1[i] > T2[i])			//miss
		{
			H.ChildH[i] = false;
		}
		else if(T2[i] < 0)				//behind, miss
		{
			H.ChildH[i] = false;
		}
		//else if(T1[i] > 0)				//hit
		//{
		//	H.ChildH[i] = true;
		//	H.T1[i] = T1[i];
		//	H.T2[i] = T2[i];
		//}
		//else
		//{								//origin inside box
		//	H.ChildH[i] = true;
		//	H.T1[i] = 0;
		//	H.T2[i] = T2[i];
		//}
		else
		{
			H.ChildH[i] = true;
		}
	}
	return H;
}

float CheckCost(int I1, int I2, std::vector<int> &TrigI, int &Best, std::vector<Triangle> &Trigs)
{
	Vec3f Min1 = {FLT_MAX, FLT_MAX, FLT_MAX};
	Vec3f Max1 = {-FLT_MAX, -FLT_MAX, -FLT_MAX};
	Vec3f Min2 = Min1;
	Vec3f Max2 = Max1;

	float Cost = FLT_MAX;
	std::vector<float> LSA(I2-I1);			//contain sizes of aabbs
	std::vector<float> RSA(I2-I1);

	int NumberOfTriangles = I2-I1;

	for(int i = 0; i < NumberOfTriangles; ++i)
	{
		auto &T = Trigs[TrigI[i+I1]];
		Vec3f Val = T.Min();
		if(Val.x < Min1.x)
			Min1.x = Val.x;
		if(Val.y < Min1.y)
			Min1.y = Val.y;
		if(Val.z < Min1.z)
			Min1.z = Val.z;

		Val = T.Max();
		if(Val.x > Max1.x)
			Max1.x = Val.x;
		if(Val.y > Max1.y)
			Max1.y = Val.y;
		if(Val.z > Max1.z)
			Max1.z = Val.z;

		LSA[i] = (Max1.x - Min1.x)*(Max1.y - Min1.y)*2  + (Max1.x - Min1.x)*(Max1.z - Min1.z)*2 + (Max1.y - Min1.y)*(Max1.z - Min1.z)*2;		//surface area of aabb
	}
	for(int i = NumberOfTriangles-1; i >= 0; --i)
	{
		auto &T = Trigs[TrigI[i+I1]];
		Vec3f Val = T.Min();
		if(Val.x < Min2.x)
			Min2.x = Val.x;
		if(Val.y < Min2.y)
			Min2.y = Val.y;
		if(Val.z < Min2.z)
			Min2.z = Val.z;

		Val = T.Max();
		if(Val.x > Max2.x)
			Max2.x = Val.x;
		if(Val.y > Max2.y)
			Max2.y = Val.y;
		if(Val.z > Max2.z)
			Max2.z = Val.z;

		RSA[i] = (Max2.x - Min2.x)*(Max2.y - Min2.y)*2  + (Max2.x - Min2.x)*(Max2.z - Min2.z)*2 + (Max2.y - Min2.y)*(Max2.z - Min2.z)*2;		//surface area of aabb
	}
	int Optimal = I2;
	for(int i = 1; i < NumberOfTriangles-1; ++i)				//dont allow splits with 0 elements in one side
	{
		float C = LSA[i]*i + RSA[i]*(NumberOfTriangles-i);
		if(C < Cost)
		{
			Cost = C;
			Optimal = i+I1;
		}
	}
	Best = Optimal;
	return Cost;
}

void
PathTracer::ConstructHierarchy(std::vector<Triangle> &Triangles)
{
	Vec3f Min = {FLT_MAX, FLT_MAX, FLT_MAX};
	Vec3f Max = {-FLT_MAX, -FLT_MAX, -FLT_MAX};
	Node *N;

	if(TrigI.size() == 0)			//create indexlist if it does not exsist
	{
		size_t Size = Triangles.size();
		TrigI.reserve(Size);
		TrigIx.reserve(Size);
		TrigIy.reserve(Size);
		TrigIz.reserve(Size);
		for(int i = 0; i < Size; ++i)
		{
			TrigI.push_back(i);
			TrigIx.push_back(i);
			TrigIy.push_back(i);
			TrigIz.push_back(i);
		}
	}
	if(BVH_.size() == 0)				//root node, all triangles
	{
		N = new Node;
		N->I1 = 0;
		N->I2 = (int)Triangles.size();
		BVH_.push_back(N);
	}
	else
	{
		N = BVH_.back();
	}
	size_t i = N->I1;
	while(i != N->I2)				//check for longest dimensions
	{
		auto &T = Triangles[TrigI[i]];
		Vec3f Val = T.Min();
		if(Val.x < Min.x)
			Min.x = Val.x;
		if(Val.y < Min.y)
			Min.y = Val.y;
		if(Val.z < Min.z)
			Min.z = Val.z;

		Val = T.Max();
		if(Val.x > Max.x)
			Max.x = Val.x;
		if(Val.y > Max.y)
			Max.y = Val.y;
		if(Val.z > Max.z)
			Max.z = Val.z;
		++i;
	}
	N->AABB[0] = Min;					//AABB
	N->AABB[1] = Max;

	if(N->I2 - N->I1 < 5)				//stop if there are few triangles left
	{
		N->LeftChild = NULL;
		N->RightChild = NULL;
		//std::cout << "leaf\n";
		return;
	}

	int BestX, BestY, BestZ;
	//sort objects based on their centerpoint along all axes
	std::sort(TrigIx.begin() + N->I1, TrigIx.begin()+ N->I2, [&Triangles](int i1, int i2) {return Triangles[i1].Center().x < Triangles[i2].Center().x; });	//up to not including I2
																																							//std::sort(TrigI.begin() + N->I1, TrigI.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].min().x < triangles[i2].min().x; });		//up to not including I2
	float CostX = CheckCost(N->I1, N->I2, TrigIx, BestX, Triangles);

	std::sort(TrigIy.begin() + N->I1, TrigIy.begin()+ N->I2, [&Triangles](int i1, int i2) {return Triangles[i1].Center().y < Triangles[i2].Center().y; });
	//std::sort(TrigI.begin() + N->I1, TrigI.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].min().y < triangles[i2].min().y; });
	float CostY = CheckCost(N->I1, N->I2, TrigIy, BestY, Triangles);

	std::sort(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, [&Triangles](int i1, int i2) {return Triangles[i1].Center().z < Triangles[i2].Center().z; });
	//std::sort(TrigI.begin() + N->I1, TrigI.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].min().z < triangles[i2].min().z; });
	float CostZ = CheckCost(N->I1, N->I2, TrigIz, BestZ, Triangles);

	int Split;
	if(CostX <= CostY)
	{
		if(CostX <= CostZ)	//X is the best
		{
			Split = BestX;
			std::copy(TrigIx.begin() + N->I1, TrigIx.begin()+ N->I2, TrigI.begin() + N->I1);
			std::copy(TrigIx.begin() + N->I1, TrigIx.begin()+ N->I2, TrigIy.begin() + N->I1);
			std::copy(TrigIx.begin() + N->I1, TrigIx.begin()+ N->I2, TrigIz.begin() + N->I1);
		}
		else				//Z is the best
		{
			Split = BestZ;
			std::copy(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, TrigI.begin() + N->I1);
			std::copy(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, TrigIx.begin() + N->I1);
			std::copy(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, TrigIy.begin() + N->I1);
		}
	}
	else
	{
		if(CostY <= CostZ)	//Y is the best
		{
			Split = BestY;
			std::copy(TrigIy.begin() + N->I1, TrigIy.begin()+ N->I2, TrigI.begin() + N->I1);
			std::copy(TrigIy.begin() + N->I1, TrigIy.begin()+ N->I2, TrigIx.begin() + N->I1);
			std::copy(TrigIy.begin() + N->I1, TrigIy.begin()+ N->I2, TrigIz.begin() + N->I1);
		}
		else
		{					//Z is the best
			Split = BestZ;
			std::copy(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, TrigI.begin() + N->I1);
			std::copy(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, TrigIx.begin() + N->I1);
			std::copy(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, TrigIy.begin() + N->I1);
		}
	}

	N->LeftChild = new Node;
	N->LeftChild->I1 = N->I1;
	N->LeftChild->I2 = Split;
	BVH_.push_back(N->LeftChild);
	ConstructHierarchy(Triangles);	//recursion

	N->RightChild = new Node;
	N->RightChild->I1 = Split;
	N->RightChild->I2 = N->I2;
	BVH_.push_back(N->RightChild);
	ConstructHierarchy(Triangles);	//recursion
}


void
PathTracer::ConvertToBVH4(Node *N1)
{
	Node4 *N4;

	if(BVH4_.size() == 0)				//root node, all triangles
	{
		N4 = new Node4;
		N4->I1 = 0;
		N4->I2 = (int)Triangles_.size();
		BVH4_.push_back(N4);
	}
	else
	{
		N4 = BVH4_.back();
	}
	N4->AABB[0] = N1->AABB[0];					//AABB
	N4->AABB[1] = N1->AABB[1];					//AABB

	if(N1->LeftChild)				//if not leaf node, always either two or no children
	{
		if(N1->LeftChild->LeftChild)
		{
			N4->Child1 = new Node4;
			BVH4_.push_back(N4->Child1);
			N4->Child1->I1 = N1->LeftChild->LeftChild->I1;
			N4->Child1->I2 = N1->LeftChild->LeftChild->I2;
			ConvertToBVH4(N1->LeftChild->LeftChild);		//recursion

			N4->Child2 = new Node4;
			BVH4_.push_back(N4->Child2);
			N4->Child2->I1 = N1->LeftChild->RightChild->I1;
			N4->Child2->I2 = N1->LeftChild->RightChild->I2;
			ConvertToBVH4(N1->LeftChild->RightChild);		//recursion
		}
		else
		{
			N4->Child1 = new Node4;
			BVH4_.push_back(N4->Child1);
			N4->Child1->I1 = N1->LeftChild->I1;
			N4->Child1->I2 = N1->LeftChild->I2;
			ConvertToBVH4(N1->LeftChild);		//recursion

			N4->Child2 = new Node4;				//empty child
			N4->Child2->AABB[0] = {INFINITY,INFINITY,INFINITY};			//guaranteed to miss
			N4->Child2->AABB[1] = {-INFINITY,-INFINITY,-INFINITY};
			BVH4_.push_back(N4->Child2);
		}
		
		if(N1->RightChild->LeftChild)
		{
			N4->Child3 = new Node4;
			BVH4_.push_back(N4->Child3);
			N4->Child3->I1 = N1->RightChild->LeftChild->I1;
			N4->Child3->I2 = N1->RightChild->LeftChild->I2;
			ConvertToBVH4(N1->RightChild->LeftChild);		//recursion

			N4->Child4 = new Node4;
			BVH4_.push_back(N4->Child4);
			N4->Child4->I1 = N1->RightChild->RightChild->I1;
			N4->Child4->I2 = N1->RightChild->RightChild->I2;
			ConvertToBVH4(N1->RightChild->RightChild);		//recursion
		}
		else
		{
			N4->Child3 = new Node4;
			BVH4_.push_back(N4->Child3);
			N4->Child3->I1 = N1->RightChild->I1;
			N4->Child3->I2 = N1->RightChild->I2;
			ConvertToBVH4(N1->RightChild);		//recursion

			N4->Child4 = new Node4;				//empty child
			N4->Child4->AABB[0] = {INFINITY,INFINITY,INFINITY};
			N4->Child4->AABB[1] = {-INFINITY,-INFINITY,-INFINITY};
			BVH4_.push_back(N4->Child4);
		}
	}
	else
	{
		N4->Leaf = true;
	}

}