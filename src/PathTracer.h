#pragma once
#include <vector>
#include "Vector/Vector.h"
#include "Scene.h"
#include "AreaLight.h"
#include "BVH.h"

template<class T>
std::istream& Read(std::istream &os, T &x)
{
	return os.read(reinterpret_cast<char*>(&x), sizeof(x));
}

template<class T>
std::ostream& Write(std::ostream& stream, const T& x)
{
	return stream.write(reinterpret_cast<const char*>(&x), sizeof(x));
}

struct RaycastResult
{
	const Triangle *Tri;	// The triangle that was hit.
	float t;				// Hit position is orig + t * dir.
	float u, v;				// Barycentric coordinates at the hit triangle.
	Vec3f Point;			// Hit position.
	Vec3f Origin, Dir;		// The traced ray. Convenience for tracing and visualization. This is not strictly needed.
};

struct DebugLine
{
	Vec3f v1, v2, Color;
};

struct ImageVec3
{
	Vec3f Color = {0,0,0};
	size_t Samples = 0;
};

class PathTracer
{
public:
	bool PathRender(Vec2i ImageSize, std::vector<ImageVec3> &Buffer, int SectionSize, int CurrentSection);
	bool PathRender4(Vec2i ImageSize, std::vector<ImageVec3> &Buffer, int SectionSize, int CurrentSection);
	bool RayRender(Vec2i ImageSize, std::vector<ImageVec3> &Buffer, int SectionSize, int CurrentSection);
	void ClearScene();
	void OpenScene(std::string Path, std::string Name, std::string Extension);
	
	void ConstructHierarchy(std::vector<Triangle> &Triangles);
	void ConvertToBVH4(Node *N1);
	//Vec3f CosDir(Vec3f N, Vec3f SurfaceN, float Far, Random &R);
	void GetTextureParameters(const RaycastResult &Hit, Vec3f &Diffuse, Vec3f &Normal, Vec3f &Specular);

	RaycastResult Raycast(const Vec3f& orig, const Vec3f& dir, std::vector<Node*> &ToCheck, float tMax);
	RaycastResult Raycast4(const Vec3f& orig, const Vec3f& dir, std::vector<Node4*> &ToCheck4, float tMax);
	bool ShadowRaycast4(const Vec3f& orig, const Vec3f& dir, std::vector<Node4*> &ToCheck4, float tMax);
	Vec3f PathTrace(Vec3f RayOrigin, Vec3f RayDirection, std::vector<Node*> &ToCheck, Random &R);
	Vec3f PathTrace(Vec3f RayOrigin, Vec3f RayDirection, std::vector<Node4*> &ToCheck4, Random &R);
	Vec3f RayTrace(Vec3f RayOrigin, Vec3f RayDirection, std::vector<Node*> &ToCheck);

	Vec3f DebugPath(int ImageX, int ImageY, Vec2i ImageSize, std::vector<DebugLine> &DebugLines, Random &R);
	Vec3f DebugPath4(int ImageX, int ImageY, Vec2i ImageSize, std::vector<DebugLine> &DebugLines, Random &R);
	Vec3f DebugRay(int ImageX, int ImageY, Vec2i ImageSize, std::vector<DebugLine> &DebugLines);

	AreaLight *		GetAreaLight();
	PointLight *	GetPointLight();

	Camera *		GetCamera();
	void			SetCamera(Camera Cam);

	void SaveScene(std::string Name);
	void LoadScene(std::string Name);
	void LoadBVH(std::string Name);
	void SaveBVH(std::string Name);

	std::vector<Triangle> Triangles_;
	std::vector<Material> Materials_;
	std::vector<Node*> BVH_;
	std::vector<Node4*> BVH4_;
	std::vector<int> TrigI;				//index for triangles
	std::vector<int> TrigIx;
	std::vector<int> TrigIy;
	std::vector<int> TrigIz;

	SceneData *SD_;

	//AreaLight AreaLight_;
	//PointLight PointLight_;
	//Camera Cam_;
	//size_t MaxBounces_ = 1;
	//bool RussianRoulette_ = false;
	size_t RayCount = 0;
};


