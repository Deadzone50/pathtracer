#pragma once

#ifndef __GNUC__
#pragma warning( disable : 4201 ) 
#endif

#include <sstream>

//#define FLT_MAX 1e30

union Vec3f
{
	struct
	{
		float x;
		float y;
		float z;
	};
	float V[3];
};

union Vec3i
{
	struct
	{
		int x;
		int y;
		int z;
	};
	struct
	{
		int P1;
		int P2;
		int P3;
	};
	int V[3];
};

struct Vec4f
{
	float x;
	float y;
	float z;
	float w;

	Vec3f GetXYZ();
};

struct Vec2f
{
	float x;
	float y;
};

struct Vec2V3
{
	Vec3f V1;
	Vec3f V2;
};

struct Vec2V2
{
	Vec2f V1;
	Vec2f V2;
};

struct Vec2i
{
	int x;
	int y;
};

Vec3f NORMALIZE(Vec3f V);
Vec2f NORMALIZE(Vec2f V);
Vec3f CROSS(Vec3f A, Vec3f B);
float DOT(Vec3f V1, Vec3f V2);
float DOT(Vec2f V1, Vec2f V2);
float SUM(Vec3f V);
float LEN(Vec3f V);
float LEN(Vec2f V);
Vec2f MINMAX(float f1, float f2);
Vec3f ABS(Vec3f V);

Vec3f operator*(float f, const Vec3f &V);
Vec3f operator*(const Vec3f &V, float f);
Vec3f operator*(float f, const Vec3i &V);
Vec3f operator*(const Vec3i &V, float f);
Vec3f operator/(const Vec3f &V, float f);
Vec3f operator/(const Vec3i &V, float f);
Vec3f operator*(int i, const Vec3f &V);
Vec3f operator*(const Vec3f &V1, const Vec3f &V2);
Vec3f operator+(const Vec3f &V1, const Vec3f &V2);
Vec3f operator-(const Vec3f &V1, const Vec3f &V2);
Vec3f operator-(const Vec3f &V1);
void  operator+=(Vec3f &V1, const Vec3f &V2);
void  operator+=(Vec3i &V1, const Vec3i &V2);
void  operator-=(Vec3f &V1, const Vec3f &V2);
void  operator-=(Vec3i &V1, const Vec3i &V2);
void  operator*=(Vec3f &V1, const Vec3f &V2);
bool  operator==(const Vec3f &V1, const Vec3f &V2);

std::ostream& operator<<(std::ostream& os, const Vec3f &V);
std::istream& operator>>(std::istream& is, Vec3f &V);

std::ostream& operator<<(std::ostream& os, const Vec2f &V);
std::istream& operator>>(std::istream& is, Vec2f &V);

std::ostream& operator<<(std::ostream& os, const Vec2i &V);
std::istream& operator>>(std::istream& is, Vec2i &V);

Vec2f operator*(float f, const Vec2f &V);
Vec2f operator/(const Vec2f &V, float f);
Vec2f operator*(const Vec2f &V1, const Vec2f &V2);
Vec2f operator+(const Vec2f &V1, const Vec2f &V2);
Vec2f operator-(const Vec2f &V1, const Vec2f &V2);
Vec2f operator-(const Vec2f &V1);
void  operator+=(Vec2f &V1, const Vec2f &V2);
void  operator-=(Vec2f &V1, const Vec2f &V2);
void  operator*=(Vec2f &V1, const Vec2f &V2);
bool  operator==(const Vec2f &V1, const Vec2f &V2);

Vec2i operator*(const int i, const Vec2i &V1);

inline Vec2f Rotate90CW(Vec2f V) {return(Vec2f{V.y, -V.x});};
inline Vec2f Rotate90CCW(Vec2f V) {return(Vec2f{-V.y, V.x});};

