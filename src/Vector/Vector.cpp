#include "Vector.h"

#include <cmath>
#include <string>
#include <sstream>

Vec2f MINMAX(float f1, float f2)
{
	if(f1 < f2)
		return Vec2f{f1,f2};
	else
		return Vec2f{f2,f1};
}

Vec3f NORMALIZE(Vec3f V)
{
	float OneOverLen = 1/sqrtf(V.x*V.x + V.y*V.y + V.z*V.z);
	Vec3f VN ={OneOverLen*V.x, OneOverLen*V.y, OneOverLen*V.z};
	return VN;
}
Vec2f NORMALIZE(Vec2f V)
{
	float OneOverLen = 1/sqrt(V.x*V.x + V.y*V.y);
	Vec2f VN ={OneOverLen*V.x, OneOverLen*V.y};
	return VN;
}

float LEN(Vec3f V)
{
	return sqrt(V.x*V.x + V.y*V.y + V.z*V.z);
}

float LEN(Vec2f V)
{
	return sqrt(V.x*V.x + V.y*V.y);
}

float SUM(Vec3f V)
{
	return V.x+V.y+V.z;
}

Vec3f CROSS(Vec3f A, Vec3f B)
{
	Vec3f C{A.y*B.z-A.z*B.y, A.z*B.x-A.x*B.z, A.x*B.y-A.y*B.x};
	return C;
}

float DOT(Vec3f V1, Vec3f V2)
{
	float f = V1.x*V2.x + V1.y*V2.y + V1.z*V2.z;
	return f;
}
float DOT(Vec2f V1, Vec2f V2)
{
	float f = V1.x*V2.x + V1.y*V2.y;
	return f;
}

Vec3f ABS(Vec3f V)
{
	Vec3f v;
	v.x = abs(V.x);
	v.y = abs(V.y);
	v.z = abs(V.z);
	return v;
}
Vec3f operator*(float f, const Vec3f &V1)
{
	Vec3f V;
	V.x=f*V1.x;
	V.y=f*V1.y;
	V.z=f*V1.z;
	return V;
}
Vec3f operator*(const Vec3f &V1, float f)
{
	return f*V1;
}
Vec3f operator/(const Vec3f &V1, float f)
{
	Vec3f V;
	V.x=V1.x/f;
	V.y=V1.y/f;
	V.z=V1.z/f;
	return V;
}
Vec3f operator/(const Vec3i & V1, float f)
{
	Vec3f V;
	V.x=V1.x/f;
	V.y=V1.y/f;
	V.z=V1.z/f;
	return V;
}
Vec3f operator*(int i, const Vec3f &V1)
{
	Vec3f V;
	V.x=i*V1.x;
	V.y=i*V1.y;
	V.z=i*V1.z;
	return V;
}
Vec3f operator*(float f, const Vec3i &V1)
{
	Vec3f V;
	V.x=f*V1.x;
	V.y=f*V1.y;
	V.z=f*V1.z;
	return V;
}
Vec3f operator*(const Vec3i &V1, float f)
{
	return f*V1;
}
Vec3f operator*(const Vec3f &V1, const Vec3f &V2)
{
	Vec3f V;
	V.x = V1.x*V2.x;
	V.y = V1.y*V2.y;
	V.z = V1.z*V2.z;
	return V;
}
void operator*=(Vec3f &V1, const Vec3f &V2)
{
	V1.x *= V2.x;
	V1.y *= V2.y;
	V1.z *= V2.z;
}
void operator+=(Vec3f &V1, const Vec3f &V2)
{
	V1.x += V2.x;
	V1.y += V2.y;
	V1.z += V2.z;
}
void operator+=(Vec3i & V1, const Vec3i & V2)
{
	V1.x += V2.x;
	V1.y += V2.y;
	V1.z += V2.z;
}
void operator-=(Vec3f &V1, const Vec3f &V2)
{
	V1.x -= V2.x;
	V1.y -= V2.y;
	V1.z -= V2.z;
}
void operator-=(Vec3i &V1, const Vec3i &V2)
{
	V1.x -= V2.x;
	V1.y -= V2.y;
	V1.z -= V2.z;
}
Vec3f operator+(const Vec3f &V1, const Vec3f &V2)
{
	Vec3f V;
	V.x = V1.x+V2.x;
	V.y = V1.y+V2.y;
	V.z = V1.z+V2.z;
	return V;
}

Vec3f operator-(const Vec3f &V1, const Vec3f &V2)
{
	Vec3f V;
	V.x = V1.x-V2.x;
	V.y = V1.y-V2.y;
	V.z = V1.z-V2.z;
	return V;
}
Vec3f operator-(const Vec3f &V1)
{
	Vec3f V;
	V.x = -V1.x;
	V.y = -V1.y;
	V.z = -V1.z;
	return V;
}

std::ostream& operator<<(std::ostream &os, const Vec3f &V)
{
  os << V.x << " , " << V.y << " , " << V.z;
  return os;
}

std::istream& operator>>(std::istream &is, Vec3f &V)
{
	is >> V.x >> V.y >> V.z;
	return is;
}

std::ostream& operator<<(std::ostream &os, const Vec2f &V)
{
	os << V.x << " , " << V.y;
	return os;
}

std::istream& operator>>(std::istream &is, Vec2f &V)
{
	is >> V.x >> V.y;
	return is;
}

std::ostream& operator<<(std::ostream &os, const Vec2i &V)
{
	os << V.x << " , " << V.y;
	return os;
}

std::istream& operator>>(std::istream &is, Vec2i &V)
{
	is >> V.x >> V.y;
	return is;
}

bool
operator==(const Vec3f &V1, const Vec3f &V2)
{
	if(V1.x == V2.x)
		if(V1.y == V2.y)
			if(V1.z == V2.z)
				return true;
	return false;
}
bool
operator==(const Vec2f &V1, const Vec2f &V2)
{
	if(V1.x == V2.x)
		if(V1.y == V2.y)
			return true;
	return false;
}

Vec2f operator*(float f, const Vec2f &V1)
{
	Vec2f V;
	V.x=f*V1.x;
	V.y=f*V1.y;
	return V;
}
Vec2f operator/(const Vec2f &V1, float f)
{
	Vec2f V;
	V.x=V1.x/f;
	V.y=V1.y/f;
	return V;
}
Vec2f operator*(const Vec2f &V1, const Vec2f &V2)
{
	Vec2f V;
	V.x = V1.x*V2.x;
	V.y = V1.y*V2.y;
	return V;
}
void operator*=(Vec2f &V1, const Vec2f &V2)
{
	V1.x *= V2.x;
	V1.y *= V2.y;
}
void operator+=(Vec2f &V1, const Vec2f &V2)
{
	V1.x += V2.x;
	V1.y += V2.y;
}
void operator-=(Vec2f & V1, const Vec2f & V2)
{
	V1.x -= V2.x;
	V1.y -= V2.y;
}
Vec2f operator+(const Vec2f &V1, const Vec2f &V2)
{
	Vec2f V;
	V.x = V1.x+V2.x;
	V.y = V1.y+V2.y;
	return V;
}

Vec2f operator-(const Vec2f &V1, const Vec2f &V2)
{
	Vec2f V;
	V.x = V1.x-V2.x;
	V.y = V1.y-V2.y;
	return V;
}
Vec2f operator-(const Vec2f &V1)
{
	Vec2f V;
	V.x = -V1.x;
	V.y = -V1.y;
	return V;
}

Vec2i operator*(const int i, const Vec2i &V1)
{
	Vec2i V;
	V.x = V1.x*i;
	V.y = V1.y*i;
	return V;
}

Vec3f
Vec4f::GetXYZ()
{
	Vec3f Result;
	Result.x = x;
	Result.y = y;
	Result.z = z;
	return Result;
}
