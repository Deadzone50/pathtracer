#pragma once

#include <sstream>
#include "Vector.h"

struct Mat3f
{
	Mat3f() {Identity();}
	float m[9];
	void SetCol(int C, Vec3f Col);
	void SetRow(int R, Vec3f Row);
	Vec3f GetCol(int C);
	Vec3f GetRow(int R);
	Mat3f Rotate(const Vec3f& R);
	void Identity();
};

struct Mat4f
{
	Mat4f() {Identity();}
	float m[16];
	void SetCol(int C, Vec4f Col);
	void SetRow(int R, Vec4f Row);
	Vec4f GetCol(int C);
	Vec4f GetRow(int R);
	Mat4f Rotate(const Vec3f& R);
	Mat3f GetXYZ();
	void Identity();
};

std::ostream& operator<<(std::ostream& os, const Mat4f& M);
Mat4f operator*(const Mat4f& M1, const Mat4f& M2);

Mat4f INVERT(const Mat4f& M);
Mat4f Transpose(const Mat4f& M);

std::ostream& operator<<(std::ostream& os, const Mat3f& M);
Mat3f operator*(const Mat3f& M1, const Mat3f& M2);
Mat3f operator*(float f, const Mat3f& M);
Mat4f operator*(float f, const Mat4f& M);
Mat3f operator*(const Mat3f& M, float f);
Vec3f operator*(const Mat3f& M, const Vec3f& V);

Vec4f operator*(const Mat4f& M, const Vec4f& V);
