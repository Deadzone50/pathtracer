#include "Matrix.h"

#include <sstream>
#include <iostream>
#include <cmath>

std::ostream& operator<<(std::ostream& os, const Mat4f& M)
{
  os << M.m[0] << "," << M.m[1] << "," << M.m[2] << "," << M.m[3] << std::endl;
  os << M.m[4] << "," << M.m[5] << "," << M.m[6] << "," << M.m[7] << std::endl;
  os << M.m[8] << "," << M.m[9] << "," << M.m[10] << "," << M.m[11] << std::endl;
  os << M.m[12] << "," << M.m[13] << "," << M.m[14] << "," << M.m[15] << std::endl;
  return os;
}

Mat4f operator*(const Mat4f &M1, const Mat4f &M2)
{
	Mat4f M;
	for(int R = 0; R < 4;++R)
		for(int C = 0; C < 4;++C)
		{
			M.m[R*4 + C] = M1.m[R*4 + 0]*M2.m[0 + C] + M1.m[R*4 + 1]*M2.m[4 + C] + M1.m[R*4 + 2]*M2.m[8 + C] + M1.m[R*4 + 3]*M2.m[12 + C];
		}
	return M;
}
void
Mat4f::Identity()
{
	SetRow(0,Vec4f{1,0,0,0});
	SetRow(1,Vec4f{0,1,0,0});
	SetRow(2,Vec4f{0,0,1,0});
	SetRow(3,Vec4f{0,0,0,1});
}


Mat4f INVERT(const Mat4f &M)		//inverses the transformation matrix without doing heavy computing, works for affine transforms
{
	Mat4f Inv;
	Inv.SetCol(0, Vec4f{M.m[0], M.m[1], M.m[2], 0});		//3x3 rotation matrix inverted (transposed)
	Inv.SetCol(1, Vec4f{M.m[4], M.m[5], M.m[6], 0});
	Inv.SetCol(2, Vec4f{M.m[8], M.m[9], M.m[10], 0});
	Inv.SetCol(3, Vec4f{-M.m[3], -M.m[7], -M.m[11], 1});	//inverse transpose
	return Inv;
}

Mat4f Transpose(const Mat4f &M)
{
	Mat4f T;
	T.SetCol(0, Vec4f{M.m[0], M.m[1], M.m[2], M.m[3]});
	T.SetCol(1, Vec4f{M.m[4], M.m[5], M.m[6], M.m[7]});
	T.SetCol(2, Vec4f{M.m[8], M.m[9], M.m[10], M.m[11]});
	T.SetCol(3, Vec4f{M.m[12], M.m[13], M.m[14], M.m[15]});
	return T;
}

void Mat4f::SetCol(int C, Vec4f Col)
{
	m[0+C] = Col.x;
	m[4+C] = Col.y;
	m[8+C] = Col.z;
	m[12+C] = Col.w;
}
void Mat4f::SetRow(int R, Vec4f Row)
{
	m[R*4+0] = Row.x;
	m[R*4+1] = Row.y;
	m[R*4+2] = Row.z;
	m[R*4+3] = Row.w;
}

Vec4f Mat4f::GetCol(int C)
{
	Vec4f Result;
	Result.x = m[0+C];
	Result.y = m[4+C];
	Result.z = m[8+C];
	Result.w = m[12+C];
	return Result;
}

Vec4f Mat4f::GetRow(int R)
{
	Vec4f Result;
	Result.x = m[R*4+0];
	Result.y = m[R*4+1];
	Result.z = m[R*4+2];
	Result.w = m[R*4+3];
	return Result;
}

Mat4f Mat4f::Rotate(const Vec3f& R)
{
	Mat4f Rot;
	Mat4f Rotated = *this;
	Rot.SetRow(0,Vec4f{1,0,0,0});					//x-axis
	Rot.SetRow(1,Vec4f{0,cosf(R.x),sinf(R.x),0});
	Rot.SetRow(2,Vec4f{0,-sinf(R.x),cosf(R.x),0});
	Rot.SetRow(3,Vec4f{0,0,0,1});
	Rotated = Rot * Rotated; 
	
	//std::cout << Rotated << std::endl;
	
	Rot.SetRow(0,Vec4f{cosf(R.y),0,-sinf(R.y),0});	//y-axis
	Rot.SetRow(1,Vec4f{0,1,0,0});
	Rot.SetRow(2,Vec4f{sinf(R.y),0,cosf(R.y),0});
	Rot.SetRow(3,Vec4f{0,0,0,1});
	Rotated = Rot * Rotated;
	
	//std::cout << Rotated << std::endl;
	
	Rot.SetRow(0,Vec4f{cosf(R.z),sinf(R.z),0,0});		//z-axis
	Rot.SetRow(1,Vec4f{-sinf(R.z),cosf(R.z),0,0});
	Rot.SetRow(2,Vec4f{0,0,1,0});
	Rot.SetRow(3,Vec4f{0,0,0,1});
	Rotated = Rot * Rotated;
	
	//std::cout << Rotated << std::endl;
	
	for (int i = 0; i < 16; ++i)
	{
		m[i] = Rotated.m[i];
	}
	return *this;
}

Mat3f Mat4f::GetXYZ()
{
	Mat3f Result;
	for(int i = 0; i < 3; ++i)
	{
		Result.SetCol(i, GetCol(i).GetXYZ());
	}
	return Result;
}


std::ostream& operator<<(std::ostream& os, const Mat3f& M)
{
	os << M.m[0] << "," << M.m[1] << "," << M.m[2] << std::endl;
	os << M.m[3] << "," << M.m[4] << "," << M.m[5] << std::endl;
	os << M.m[6] << "," << M.m[7] << "," << M.m[8] << std::endl;
	return os;
}

Mat3f Invert(const Mat3f &M)		//inverses the transformation matrix without doing heavy computing, works for affine transforms
{
	Mat3f Inv;
	Inv.SetCol(0, Vec3f{M.m[0], M.m[1], M.m[2]});		//3x3 rotation matrix inverted (transposed)
	Inv.SetCol(1, Vec3f{M.m[3], M.m[4], M.m[5]});
	Inv.SetCol(2, Vec3f{M.m[6], M.m[7], M.m[8]});
	return Inv;
}

Mat3f Transpose(const Mat3f &M)
{
	Mat3f T;
	T.SetCol(0, Vec3f{M.m[0], M.m[1], M.m[2]});		//3x3 rotation matrix inverted (transposed)
	T.SetCol(1, Vec3f{M.m[3], M.m[4], M.m[5]});
	T.SetCol(2, Vec3f{M.m[6], M.m[7], M.m[8]});
	return T;
}

void Mat3f::SetCol(int C, Vec3f Col)
{
	m[0+C] = Col.x;
	m[3+C] = Col.y;
	m[6+C] = Col.z;
}
void Mat3f::SetRow(int R, Vec3f Row)
{
	m[R*3+0] = Row.x;
	m[R*3+1] = Row.y;
	m[R*3+2] = Row.z;
}

Vec3f Mat3f::GetCol(int C)
{
	Vec3f Result;
	Result.x = m[0+C];
	Result.y = m[3+C];
	Result.z = m[6+C];
	return Result;
}

Vec3f Mat3f::GetRow(int R)
{
	Vec3f Result;
	Result.x = m[R*3+0];
	Result.y = m[R*3+1];
	Result.z = m[R*3+2];
	return Result;
}

Mat3f Mat3f::Rotate(const Vec3f& R)
{
	Mat3f Rot;
	Mat3f Rotated = *this;
	Rot.SetRow(0, Vec3f{1,0,0});					//x-axis
	Rot.SetRow(1, Vec3f{0,cosf(R.x),sinf(R.x)});
	Rot.SetRow(2, Vec3f{0,-sinf(R.x),cosf(R.x)});
	Rotated = Rot * Rotated;

	//std::cout << Rotated << std::endl;

	Rot.SetRow(0, Vec3f{cosf(R.y),0,-sinf(R.y)});	//y-axis
	Rot.SetRow(1, Vec3f{0,1,0});
	Rot.SetRow(2, Vec3f{sinf(R.y),0,cosf(R.y)});
	Rotated = Rot * Rotated;

	//std::cout << Rotated << std::endl;

	Rot.SetRow(0, Vec3f{cosf(R.z),sinf(R.z),0});		//z-axis
	Rot.SetRow(1, Vec3f{-sinf(R.z),cosf(R.z),0});
	Rot.SetRow(2, Vec3f{0,0,1});
	Rotated = Rot * Rotated;

	//std::cout << Rotated << std::endl;

	for(int i = 0; i < 9; ++i)
	{
		m[i] = Rotated.m[i];
	}
	return *this;
}
void
Mat3f::Identity()
{
	SetRow(0, Vec3f{1,0,0});
	SetRow(1, Vec3f{0,1,0});
	SetRow(2, Vec3f{0,0,1});
}

Mat3f operator*(const Mat3f &M1, const Mat3f &M2)
{
	Mat3f M;
	for(int R = 0; R < 3; ++R)
		for(int C = 0; C < 3; ++C)
		{
			M.m[R*3 + C] = M1.m[R*3 + 0]*M2.m[0 + C] + M1.m[R*3 + 1]*M2.m[3 + C] + M1.m[R*3 + 2]*M2.m[6 + C];
		}
	return M;
}

Vec3f operator*(const Mat3f& M, const Vec3f& V)
{
	Vec3f Vec;
	Vec.x = M.m[0]*V.x + M.m[1]*V.y + M.m[2]*V.z;
	Vec.y = M.m[3]*V.x + M.m[4]*V.y + M.m[5]*V.z;
	Vec.z = M.m[6]*V.x + M.m[7]*V.y + M.m[8]*V.z;
	return Vec;
}
Vec4f operator*(const Mat4f& M, const Vec4f& V)
{
	Vec4f Vec;
	Vec.x = M.m[0]*V.x + M.m[1]*V.y + M.m[2]*V.z + M.m[3]*V.w;
	Vec.y = M.m[4]*V.x + M.m[5]*V.y + M.m[6]*V.z + M.m[7]*V.w;
	Vec.z = M.m[8]*V.x + M.m[9]*V.y + M.m[10]*V.z + M.m[11]*V.w;
	Vec.w = M.m[12]*V.x + M.m[13]*V.y + M.m[14]*V.z + M.m[15]*V.w;
	return Vec;
}
Mat3f operator*(float f, const Mat3f& M)
{
	Mat3f F;
	for(int R = 0; R < 3; ++R)
		for(int C = 0; C < 3; ++C)
		{
			F.m[R*3 + C] = f*M.m[R*3 + C];
		}
	return F;
}
Mat4f operator*(float f, const Mat4f& M)
{
	Mat4f F;
	for(int R = 0; R < 4; ++R)
		for(int C = 0; C < 4; ++C)
		{
			F.m[R*4 + C] = f*M.m[R*4 + C];
		}
	return F;
}
Mat3f operator*(const Mat3f& M, float f)
{
	Mat3f F;
	for(int R = 0; R < 3; ++R)
		for(int C = 0; C < 3; ++C)
		{
			F.m[R*3 + C] = f*M.m[R*3 + C];
		}
	return F;
}