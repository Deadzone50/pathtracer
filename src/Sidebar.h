#pragma once
#include <vector>
#include "Vector/Vector.h"
#include "Scene.h"

struct Button
{
	Vec2f Min, Max;
	std::string Text;

	std::string Click(float x, float y)
	{
		if(Min.x < x && x < Max.x)
		{
			if(Min.y < y && y < Max.y)
			{
				return Text;
			}
		}
		return "miss";
	}

};

struct Textbox
{
	Vec2f Min, Max;
	std::string Text;
	int *Value;
};


class Sidebar
{
public:
	void CreateSidebar(SceneData &Data);
	void Sidebar::DrawSidebar(Vec2i &ScreenSize, int SideBarWidth);

	std::vector<Button> MenuButtons;
	std::vector<Textbox> MenuText;
};