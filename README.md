This is a remake of the path tracer I did for school. It uses glut(Freeglut30) and openmp.

I decided to remake it from scratch, and wrote my own vector and matrix classes.

It is a Pathtracer/Raytracer that can load obj files as scenes, and reads materials from mtl files. It creates a Bounding volume hierarchy with surface area heuristics.

q quits the program.

wasd controls the camera, and right click on the mouse turns it. Mousewheel changes the speed of the cameramovement

e starts pathtracing.
r starts raytracing. The raytracer uses a pointlight.
t starts pathtracing using a BVH4 tree.

E, R, T stops all tracing.

1-5 changes scenes.

space sets the light at the current camera position.

left click shoots a debug ray in the scene.